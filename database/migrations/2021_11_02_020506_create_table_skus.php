<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSkus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skus', function (Blueprint $table) {
            $table->integer('skuid');
            $table->string('sku_name');
            $table->string('is_active');
            $table->string('ean');
            $table->integer('product_id');
            $table->string('product_name');
            $table->string('product_ref_id');
            $table->string('departament_name');
            $table->string('category_name');
            $table->string('brand');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skus');
    }
}
