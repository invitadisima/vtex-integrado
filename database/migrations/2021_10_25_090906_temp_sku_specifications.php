<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TempSkuSpecifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('temp_sku_specifications', function (Blueprint $table) {
                        $table->string('Id');
                        $table->string('SkuId');
                        $table->string('FieldId');
                        $table->string('FieldValueId');
                        $table->string('Text');
                    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
    {
        Schema::dropIfExists('temp_sku_specifications');
    }
}
