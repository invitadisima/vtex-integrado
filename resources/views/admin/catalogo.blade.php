@if(!empty($data) && count($data))



<div class="row">
    <div class="col-8">
        <div class="form-group">
            <label for="tipo_equipo" class="col-4 control-label">Campos a Actualizar:</label>
            <div class="input-group input-group-sm">
                <select id="selecCampos" multiple class="form-control-lg" style="width: 100%">
                    @foreach($campos as $campo)
                    <option <?php
                    if ($campo[0] == 'Id') {
                        echo 'selected';
                    } else if ($campo[0] == 'Name') {
                        echo 'selected';
                    }if ($campo[0] == 'ProductId') {
                        echo 'selected';
                    } else if ($campo[0] == 'ProductRefId') {
                        echo 'selected';
                    }
                    ?> value="{{$campo[0]}}">{{$campo[1]}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-4 pt-4 mt-2">

        <div class="input-group-append" id="button-addon2">
            <button id="GenerarExcelUdp" class="btn btn-primary square" type="button"><i class="ft-upload mr-1"></i>Descarga de archivo para actualizaciones</button>
        </div>
        <button id="botonLoadingExcelUdp" class="btn btn-primary" type="button" disabled>
            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
            Procesando...
        </button>


        <!--<a href="javascript()" id="GenerarExcelUdp" class="btn btn-primary">Generar Archivo de Actualizaciones</a>-->

    </div>
</div>
<div class=" card-content table-responsive">

    <table id="datatableCatalogo" class="table table-striped table-bordered table-condensed" style="width:100%">
        <thead>
        <th width="3%"><input type="checkbox" id="checkAll"></th>

        @foreach($columns as $col)

        <th>{{$col}}</th>


        @endforeach



        </thead>
        <tbody>

            @foreach($data as $row)
            <tr>
                <td><input data-productIds="{{ $row['ProductId'] }}" data-skuNames="{{ $row['SkuName'] }}"  type="checkbox" class="checkbox" name="checkbox[]" value="{{ $row['SkuId'] }}"></td>

                @foreach($columns as $col)

                <td>{{$row[$col]}}</td>


                @endforeach

            </tr>
            @endforeach

        </tbody>
    </table>

</div>

@else
<h2> Sin datos que mostrar</h2>
@endif