@extends('adminlte::page')

@section('title', 'Admin Vtex API CONSUMER')


@section('content_header')
<h1>Actualización de PRODUCTOS </h1>
@stop

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bgsize-primary-4 white card-header">
                    <div class="row">
                        <h4 class="card-title">Filtros </h4>
                    </div>
                    <div class="row"> 
                        <div class="pull-left col-6">
                            <div class="form-group">
                                <label for="tipo_equipo" class="col-sm-4 control-label">Categorias:</label>
                                <div class="col-sm-12">
                                    <div class="input-group input-group-sm">
                                        <select id="category-filter" name="category-filter" class="form-control" style="width: 100%">
                                            <option value="">Seleccione una Categoria</option>
                                            @foreach($categorias as $categoria)
                                            <option data-name="{{$categoria['name']}}" value="{{$categoria['id']}}">{{$categoria['ruta']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pull-left col-6">
                            <div class="form-group">
                                <label for="tipo_equipo" class="col-sm-4 control-label">Marca:</label>
                                <div class="col-sm-12">
                                    <div class="input-group input-group-sm">
                                        <select id="marca-filter" name="marca-filter" class="form-control" style="width: 100%">
                                            <option value="">Seleccione una Marca</option>
                                            @foreach($marcas as $marca)
                                            <option value="{{$marca['id']}}">{{$marca['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!--                <div class="card-body">
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                
                                    @if(session()->has('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div>
                                    @endif
                
                                    <form id="formImportarCatalogo" action="{{url("udp_catalogo/import")}}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <fieldset>
                                            <label>Cargar los datos iniciales dese archivo <small class="warning text-muted">{{__('Solo archivos Excel (.xlsx or .xls)')}}</small></label>
                                            <div class="input-group">
                                                <input type="file" required class="form-control" name="producto_file" id="producto_file">
                                                <div class="input-group-append" id="button-addon2">
                                                    <button id="procesarImportarCatalogo" class="btn btn-primary square" type="button"><i class="ft-upload mr-1"></i>Cargar</button>
                                                </div>
                                                <button id="botonLoadingimportar" class="btn btn-primary" type="button" disabled>
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    Procesando...
                                                </button>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>-->
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bgsize-primary-4 white card-header">
                    <h4 class="card-title">Productos</h4>
                </div>
                <div class="card-body" id="tablaCatalogo">
                    <div class="row">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="tipo_equipo" class="col-4 control-label">Campos a Actualizar:</label>
                                <div class="input-group input-group-sm">
                                    <select id="selecCampos" multiple class="form-control-lg" style="width: 100%">
                                        @foreach($campos as $campo)
                                        <option <?php
                                        if ($campo[0] == 'Name') {
                                            echo 'selected';
                                            echo ' ';
                                            echo 'locked="locked"';
                                        }if ($campo[0] == 'Id') {
                                            echo 'selected';
                                            echo ' ';
                                            echo 'locked="locked"';
                                        } else if ($campo[0] == 'RefId') {
                                            echo 'selected';
                                            echo ' ';
                                            echo 'locked="locked"';
                                        } else if ($campo[0] == 'CategoryId') {
                                            echo 'selected';
                                            echo ' ';
                                            echo 'locked="locked"';
                                        } else if ($campo[0] == 'BrandId') {
                                            echo 'selected';
                                            echo ' ';
                                            echo 'locked="locked"';
                                        }
                                        ?> value="{{$campo[0]}}">{{$campo[1]}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 pt-4 mt-2">

                            <div class="input-group-append" id="button-addon2">
                                <button id="GenerarExcelUdp" class="btn btn-primary square" type="button"><i class="ft-upload mr-1"></i>Descarga de archivo para actualizaciones</button>
                            </div>
                            <button id="botonLoadingExcelUdp" class="btn btn-primary" type="button" disabled>
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                Procesando...
                            </button>


                            <!--<a href="javascript()" id="GenerarExcelUdp" class="btn btn-primary">Generar Archivo de Actualizaciones</a>-->

                        </div>
                    </div>


                    <table id="datatableCatalogo" class="table table-striped table-bordered table-condensed" style="width:100%">
                        <thead>

                        <th width="3%"><input type="checkbox" id="checkAll"></th>
                        <th>Product_Id</th>
                        <th>Product_Name</th>
                        <th>Product_RefId</th>
                        <th>Departament_id</th>
                        <th>Departament</th>
                        <th>Category_id</th>
                        <th>Category</th>
                        <th>Brand_id</th>
                        <th>Brand</th>
                        </thead>
                        <tbody>

                            @foreach($productos as $producto)
                            <tr>
                                <td><input data-productIds="{{ $producto->product_id }}"  type="checkbox" class="checkbox" name="checkbox[]" value="{{ $producto->product_id }}"></td>
                                <td>{{$producto->product_id}}</td>
                                <td>{{$producto->product_name}}</td>
                                <td>{{$producto->product_ref_id}}</td>
                                <td>{{$producto->departament_id}}</td>
                                <td>{{$producto->departament_name}}</td>
                                <td>{{$producto->category_id}}</td>
                                <td>{{$producto->category_name}}</td>
                                <td>{{$producto->brand_id}}</td>
                                <td>{{$producto->brand}}</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Enviar archivo de actualizaciones</h4>
                </div>
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    <br>
                    @endif
                    <form id="formEnviarUdp" action="" method="post" enctype="multipart/form-data">
                        @csrf
                        <fieldset>
                            <label>Seleccione un archivo para enviar actualización <small class="warning text-muted">{{__('Solo archivos Excel (.xlsx or .xls)')}}</small></label>
                            <div class="input-group">
                                <input type="file" required class="form-control" name="producto_udp" id="producto_udp">
                                <div class="input-group-append" id="button-addon2">
                                    <button id="enviarArchivoUDP" class="btn btn-primary square" type="button"><i class="ft-upload mr-1"></i>Enviar</button>
                                </div>
                                <button id="botonLoadinUdp" class="btn btn-primary" type="button" disabled>
                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    Procesando...
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@stop


@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    $(document).ready(function () {

        $('#botonLoadingimportar').hide();
        $('#botonLoadinUdp').hide();
        $('#botonLoadingimportar').hide();
        $('#procesarImportarCatalogo').show();
        $('#selecCampos').select2({
        }).on('select2:unselecting', function (e) {
            // before removing tag we check option element of tag and 
            // if it has property 'locked' we will create error to prevent all select2 functionality
            if ($(e.params.args.data.element).attr('locked')) {
                e.preventDefault();
                alert('Este campo es obligatorio');
            }
        });
        $('#botonLoadingExcelUdp').hide();

        var table = $('#datatableCatalogo').DataTable({

            deferRender: true,
//            dom: 'lrtip',
            scrollX: true,
            scrollY: 800,
            scrollCollapse: true,
            scroller: true,
            searching: true,
            paging: true,
            info: false,
            columnDefs: [
                {"targets": [4], "visible": false, "searchable": true},
                {"targets": [5], "visible": false, "searchable": true},
                {"targets": [6], "visible": false, "searchable": true},
                {"targets": [8], "visible": false, "searchable": true},
//                {
//                    render: function (data, type, full, meta) {
//                        return "<div class='text-wrap width-200' style=' height:50px; width:300px; overflow:auto'>" + data + "</div>";
//                    },
//                    targets: [2, 16, 18, 20, 21, 22, 23, 24, 26, 27, 29, 30, 31, 40],
//                }
            ]
        });

        $('#category-filter').select2();
        $('#marca-filter').select2();

        $('#category-filter').on('change', function () {
            //table.search(this.value).draw();
            // table.fnFilter(this.options[this.selectedIndex].text);

            var searchTerm = this.value;
            regex = '\\b' + searchTerm + '\\b';

            if (this.value != '') {
                table.columns(6).search(regex, true, false).draw();
            } else {
                table.columns(6).search("").draw();
            }
        });

        $('#marca-filter').on('change', function () {
            //table.search(this.value).draw();
            // table.fnFilter(this.options[this.selectedIndex].text);
            if (this.value != '') {
                table.columns(9).search(this.options[this.selectedIndex].text).draw();
            } else {
                table.columns(9).search('').draw();
            }
        });



//        $(document).on('click', '#checkAll', function (e) {
//            $('input:checkbox').not(this).prop('checked', this.checked);
//        });
        $(document).on('click', '#checkAll', function (e) {


            var checked = this.checked;
            table.column(0).nodes().to$().each(function (index) {
//                if (checked) {
                $(this).find('.checkbox').prop('checked', checked);
//                } else {
//                    $(this).find('.checkbox').removeProp('checked');
//                }
            });
            // $('input:checkbox').not(this).prop('checked', this.checked);
            table.draw();

        });



        $(document).on('click', '#GenerarExcelUdp', function (e) {
            e.preventDefault();
            var arrProduct = [];
            var campos = [];
            var arrProductIds = [];
            var arrProductNames = [];
            var i = 0;
            var checados = 0;

//            checados = $("input[type=checkbox]:checked").not('#checkAll').length;
            table.column(0).nodes().to$().each(function (index) {
                checados = checados + $(this).find("input[type=checkbox]:checked").not('#checkAll').length;
            });
            if (checados == 0) {
                alert('Debe seleccionar al menos un Producto');
                return
            }


//            // arrProduct = $("input[type=checkbox]:checked").not('#checkAll').serializeArray();
//            $("input[type=checkbox]:checked").not('#checkAll').each(function () {
//                arrProduct.push($(this).val());
//                arrProductIds.push($(this).data("productIds"));
//                arrProductNames.push($(this).data("productNames"));
//
//            });
            table.column(0).nodes().to$().each(function (index) {
                checado = $(this).find("input[type=checkbox]:checked").not('#checkAll');
                if (checado.length > 0) {
                    arrProduct.push(checado.val());
                    arrProductIds.push(checado[0].dataset.productids);
                    arrProductNames.push(checado[0].dataset.productnames);
                }

            });

            campos = $('#selecCampos').val();
            data = {
                arrProduct,
                campos,
                arrProductIds,
                arrProductNames
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                dataType: 'json',
                url: '/admin/udp_catalogo/getExcelProductoUdp',
                data: data,
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                cache: false,
                beforeSend: function () {
                    $('#GenerarExcelUdp').hide();
                    $('#botonLoadingExcelUdp').show();

                },
                success: function (response) {
                    $('#botonLoadingExcelUdp').hide();
                    $('#GenerarExcelUdp').show();
                    var $a = $("<a>");
                    $a.attr("href", window.location.origin + '/' + response);
                    $("body").append($a);
                    $a.attr("download", response);
                    $a[0].click();
                    $a.remove();
                },
                error: function (xhr, status, error) {
                    $('#botonLoadingExcelUdp').hide();
                    $('#GenerarExcelUdp').show();
                    alert(error);
                    alert("Status: " + status);
                    alert("Error: " + error);
                },
            });
        });

        $(document).on('click', '#enviarArchivoUDP', function (e) {

            e.preventDefault();
            var data = new FormData(document.getElementById("formEnviarUdp"));


            var arrProduct = [];
            var campos = [];
            var i = 0;



            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: '/admin/udp_catalogo/enviarProductoUDP',
                data: data,
                contentType: false,
                processData: false,
                enctype: 'multipart/form-data',

                beforeSend: function () {
                    $('#enviarArchivoUDP').hide();
                    $('#botonLoadinUdp').show();
                },
                success: function (response) {
                    $('#enviarArchivoUDP').show();
                    $('#botonLoadinUdp').hide();
                    alert(response);

                    var $a = $("<a>");
                    $a.attr("href", window.location.origin + '/' +JSON.parse(response));
                    $("body").append($a);
                    $a.attr("download", response);
                    $a[0].click();
                    $a.remove();



                },
                error: function (xhr, status, error) {
                    $('#enviarArchivoUDP').show();
                    $('#botonLoadinUdp').hide();
                    alert(error);
                    alert("Status: " + status);
                    alert("Error: " + error);
                }
            });
        });

    });




</script>
@stop