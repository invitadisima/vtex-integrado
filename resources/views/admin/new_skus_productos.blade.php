@extends('adminlte::page')

@section('title', 'Admin Vtex API CONSUMER')


@section('content_header')
<h1>Agregar Nuevos Productos y Skus</h1>
@stop

@section('content')

<div class="container">
    
   
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bgsize-primary-4 white card-header">
                    <h4 class="card-title">Catalogo de Skus</h4>
                </div>
                <div class="row">
                   
                    <div class="col-4 pt-4 mt-2">

                        <div class="input-group-append" id="button-addon2">
                            <button id="GenerarExcelUdp" class="btn btn-primary square" type="button"><i class="ft-upload mr-1"></i>Descarga de archivo para actualizaciones</button>
                        </div>
                        <button id="botonLoadingExcelUdp" class="btn btn-primary" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Procesando...
                        </button>


                        <!--<a href="javascript()" id="GenerarExcelUdp" class="btn btn-primary">Generar Archivo de Actualizaciones</a>-->

                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Enviar archivo de actualizaciones</h4>
                </div>
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    <br>
                    @endif
                    <form id="formEnviarUdp" action="" method="post" enctype="multipart/form-data">
                        @csrf
                        <fieldset>
                            <label>Seleccione un archivo para enviar actualización <small class="warning text-muted">{{__('Solo archivos Excel (.xlsx or .xls)')}}</small></label>
                            <div class="input-group">
                                <input type="file" required class="form-control" name="catalogo_udp" id="catalogo_udp">
                                <div class="input-group-append" id="button-addon2">
                                    <button id="enviarArchivoUDP" class="btn btn-primary square" type="button"><i class="ft-upload mr-1"></i>Enviar</button>
                                </div>
                                <button id="botonLoadinUdp" class="btn btn-primary" type="button" disabled>
                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    Procesando...
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@stop


@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    $(document).ready(function () {
        $('#botonLoadingimportar').hide();
        $('#botonLoadinUdp').hide();
        $('#botonLoadingimportar').hide();
        $('#procesarImportarCatalogo').show();
        $('#selecCampos').select2({}).on('select2:unselecting', function (e) {
            // before removing tag we check option element of tag and 
            // if it has property 'locked' we will create error to prevent all select2 functionality
            if ($(e.params.args.data.element).attr('locked')) {
                e.preventDefault();
                alert('Este campo es obligatorio');
            }
        });
        $('#botonLoadingExcelUdp').hide();


        $(document).on('click', '#GenerarExcelUdp', function (e) {
            e.preventDefault();
           

            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                dataType: 'json',
                url: '/admin/udp_catalogo/getExcelSkuProductNew',
                
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                cache: false,
                beforeSend: function () {
                    $('#GenerarExcelUdp').hide();
                    $('#botonLoadingExcelUdp').show();

                },
                success: function (response) {
                    $('#botonLoadingExcelUdp').hide();
                    $('#GenerarExcelUdp').show();
                    var $a = $("<a>");
                    $a.attr("href", window.location.origin + '/' + response);
                    $("body").append($a);
                    $a.attr("download", response);
                    $a[0].click();
                    $a.remove();
                },
                error: function (xhr, status, error) {
                    $('#botonLoadingExcelUdp').hide();
                    $('#GenerarExcelUdp').show();
                    alert(error);
                    alert("Status: " + status);
                    alert("Error: " + error);
                },
            });
        });

        $(document).on('click', '#enviarArchivoUDP', function (e) {

            e.preventDefault();
            var data = new FormData(document.getElementById("formEnviarUdp"));


            var arrSku = [];
            var campos = [];
            var i = 0;



            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: '/admin/udp_catalogo/enviarSkuProductoNew',
                data: data,
                contentType: false,
                processData: false,
                enctype: 'multipart/form-data',

                beforeSend: function () {
                    $('#enviarArchivoUDP').hide();
                    $('#botonLoadinUdp').show();
                },
                success: function (response) {
                    $('#enviarArchivoUDP').show();
                    $('#botonLoadinUdp').hide();

                    alert(response);

                    var $a = $("<a>");
                    $a.attr("href", window.location.origin + '/' + JSON.parse(response));
                    $("body").append($a);
                    $a.attr("download", response);
                    $a[0].click();
                    $a.remove();



                },
                error: function (xhr, status, error) {
                    $('#enviarArchivoUDP').show();
                    $('#botonLoadinUdp').hide();
                    alert(error);
                    alert("Status: " + status);
                    alert("Error: " + error);
                },
            });
        });

    });
</script>
@stop