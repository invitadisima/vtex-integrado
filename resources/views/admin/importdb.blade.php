@extends('adminlte::page')

@section('title', 'Admin Vtex API CONSUMER')


@section('content_header')
<h1>Importar Catalogo </h1>
@stop

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bgsize-primary-4 white card-header">
                    <div class="row">
                        <h4 class="card-title">Carga de datos iniciales desde Archivo Vtex</h4>
                    </div>
                    
                </div>

                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                    @endif

                    <form id="formImportarCatalogo" action="{{url("udp_catalogo/import")}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <fieldset>
                            <label>Cargar los datos iniciales desde archivo <small class="warning text-muted">{{__('Solo archivos Excel (.xlsx or .xls)')}}</small></label>
                            <div class="input-group">
                                <input type="file" required class="form-control" name="catalogo_file" id="catalogo_file">
                                <div class="input-group-append" id="button-addon2">
                                    <button id="procesarImportarCatalogo" class="btn btn-primary square" type="button"><i class="ft-upload mr-1"></i>Cargar</button>
                                </div>
                                <button id="botonLoadingimportar" class="btn btn-primary" type="button" disabled>
                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    Procesando...
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

 
</div>



@stop


@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    $(document).ready(function () {
        $('#botonLoadingimportar').hide();
        $('#botonLoadinUdp').hide();

        $(document).on('click', '#checkAll', function (e) {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        $(document).on('click', '#procesarImportarCatalogo', function (e) {

            e.preventDefault();
            var data = new FormData(document.getElementById("formImportarCatalogo"));


            var arrSku = [];
            var campos = [];
            var i = 0;



            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: '/admin/udp_catalogo/importo-to-db',
                data: data,
                contentType: false,
                processData: false,
                enctype: 'multipart/form-data',

                beforeSend: function () {
                    $('#procesarImportarCatalogo').hide();
                    $('#botonLoadingimportar').show();
                },
                success: function (response) {
                    $('#botonLoadingimportar').hide();
                    $('#procesarImportarCatalogo').show();

                  alert('Importacion Realizada Correctamente')


                },
                error: function (xhr, status, error) {
                    $('#botonLoadingimportar').hide();
                    $('#procesarImportarCatalogo').show();
                    alert(error);
                    alert("Status: " + status);
                    alert("Error: " + error);
                },
            });
        });



    });




</script>
@stop