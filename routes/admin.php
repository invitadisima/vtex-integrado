<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group( ['middleware' => 'auth' ], function()
{
    
Route::get('/', function(){
    return view('admin.home');
});

Route::get('udp_catalogo', [App\Http\Controllers\CatalogosController::class, 'index']);

Route::post('udp_catalogo/import', [App\Http\Controllers\CatalogosController::class, 'import']);

Route::post('udp_catalogo/getExcelSkuCatalogoUdp', [App\Http\Controllers\CatalogosController::class, 'getExcelSkuCatalogoUdp']);

Route::post('udp_catalogo/enviarUDP', [App\Http\Controllers\CatalogosController::class, 'enviarUDP']);


Route::get('udp_productos', [App\Http\Controllers\ProductosController::class, 'index']);

Route::post('udp_catalogo/import', [App\Http\Controllers\ProdutosController::class, 'import']);

Route::post('udp_catalogo/getExcelProductoUdp', [App\Http\Controllers\ProductosController::class, 'getExcelProductoUdp']);

Route::post('udp_catalogo/enviarProductoUDP', [App\Http\Controllers\ProductosController::class, 'enviarUDP']);


Route::get('udp_catalogo/importo-to-db', [App\Http\Controllers\CatalogosController::class, 'importToDB']);
Route::post('udp_catalogo/importo-to-db', [App\Http\Controllers\CatalogosController::class, 'importToDB']);

Route::get('udp_skus_productos', [App\Http\Controllers\SkusAndProductsController::class, 'index']);


Route::post('udp_catalogo/getExcelSkuProductUdp', [App\Http\Controllers\SkusAndProductsController::class, 'getExcelSkuProductUdp']);

Route::post('udp_catalogo/enviarSkuProductoUDP', [App\Http\Controllers\SkusAndProductsController::class, 'enviarUDP']);


Route::get('udp_catalogo/newExcelSkuProduct', [App\Http\Controllers\NewSkusAndProductsController::class, 'index']);

Route::post('udp_catalogo/getExcelSkuProductNew', [App\Http\Controllers\NewSkusAndProductsController::class, 'getExcelSkuProductNew']);

Route::post('udp_catalogo/enviarSkuProductoNew', [App\Http\Controllers\NewSkusAndProductsController::class, 'enviarUDP']);



});