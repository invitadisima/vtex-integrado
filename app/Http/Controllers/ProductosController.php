<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\SpreadsheetController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;
use App\Models\Skus;

class ProductosController extends Controller {

//estructura del campo 
//primer valor es como se llama en el api, s
//egundo valor es el nombre para mostrar en excel y el el listado, 
//terecer valor indica si es editable. 
//cuarto valor mostrar si o no
    var $campos = [
        ['Id', 'ProductoId', 0, 1], //se llama Id en el api, se muestra en la lista como Id, es editable y se muestra 
        ['Name', 'Name', 0, 1],
        ['RefId', 'RefId', 1, 1],
        ['CategoryId', 'CategoryId', 0, 1],
        ['BrandId', 'BrandId', 0, 1],
        ['Description', 'Description', 1, 1],
        ['DescriptionShort', 'DescriptionShort', 1, 1],
        ['Title', 'Title', 1, 1],
        ['MetaTagDescription', 'MetaTagDescription', 1, 1],
        ['IsActive', 'IsActive', 1, 1],
        ['IsVisible', 'IsVisible', 1, 1],
    ];
    public static $colores;
    public static $color;
    public static $talla;
    public static $tallaZapato;
    public static $material;
    public $categorias;
    public $marcas;
    public $specifications;

    public function __construct() {
        // Fetch the Site Settings object
        $this->categorias = $this->getCategorias();
        $this->marcas = $this->getMarcas();
    }

    //
    public function index() {
        // $this->categorias = json_encode($this->getCategorias(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        // $this->marcas = $this->getMarcas();
        //  dd($categorias);
        $productos = DB::table('skus')
                        ->select(
                                'product_id', 'product_name', 'product_ref_id', 'category_id', 'category_name', 'brand_id', 'brand', 'departament_id', 'departament_name'
                        )
                        ->distinct()->get()->toArray();
//        dd($productos);
        return view('admin.udp_producto')->with(['categorias' => $this->categorias, 'marcas' => $this->marcas, 'campos' => $this->campos, 'productos' => $productos]);
    }

    function import(Request $request) {
        $validated = $request->validate([
            'producto_file' => 'required|mimes:xlsx,csv,xls|max:32000',
        ]);
        $file1 = $request->file('producto_file');

        //Descomentar estas lineas si la importacion viene de un excel
        $result = app('App\Http\Controllers\SpreadsheetController')->importToJson($file1);
        $result = json_decode($result, true);

        return view('admin.producto')->with(['data' => $result['data'], 'columns' => $result['columns'], 'campos' => $this->campos]);
    }

    public static function getCategorias() {
        $baseUrl = env('API_ENDPOINT');
        $apiAcount = env('API_ACOUNTNAME');
        $apiEnviroment = env('API_ENVIROMENT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $client = new Client();
        $url = $baseUrl . "/api/catalog_system/pub/category/tree/8";

        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
        ];
        try {
            $request = $client->get($url, [
                'http_errors' => true,
                'headers' => $headers
            ]);
            $response = json_decode($request->getBody());

            //$response = self::_getTree($response);
            $list = [];
            $list = self::_getListCategory($response, '', $list, 0);
            $listCat = [];
            $response = [];
            foreach ($list as $key => $value) {
                $listCat = ['id' => $value['id'], 'name' => trim($value['name']), 'ruta' => trim($value['ruta'])];
                DB::table('categories')->upsert($listCat, ['id']);
                $response[] = $listCat;
            }
            $column = 'ruta';
            array_multisort(array_column($response, $column), $response);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $response = response()->json(['status' => 'error', 'response' => $exception]);
            } else {
                $response = response()->json(['status' => 'error', 'response' => $e->getMessage()]);
            }
        }

        return $response;
    }

    public static function _getTree($tree) {
        $i = 0;
        $arbol = [];
        foreach ($tree as $hijo) {
//         $arbol[$i]['id'] = $hijo->id;
            $arbol[$i]['id'] = $hijo->id;
            $arbol[$i]['name'] = $hijo->name;
            if ($hijo->hasChildren) {
                $arbol[$i]['nodes'] = self::_getTree($hijo->children);
            }
//            else
//                $arbol[$i]['nodes'] = [];
            $i++;
        }
        return $arbol;
    }

    public static function _getListCategory($tree, $ruta, &$list) {

        foreach ($tree as $hijo) {
            $id = $hijo->id;
            $text = $ruta . $hijo->name;
//            $list.put([$id=>$text]);
            array_push($list, ['id' => $id, 'ruta' => $text, 'name' => $hijo->name]);
            if ($hijo->hasChildren) {
                self::_getListCategory($hijo->children, $text . '/', $list);
            }
        }
        return $list;
    }

    public static function getMarcas() {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $client = new Client();
        $url = $baseUrl . "/api/catalog_system/pvt/brand/list";
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        try {
            $request = $client->get($url, [
                'http_errors' => true,
                'headers' => $headers
            ]);
            $response = json_decode($request->getBody());
            $list = (array) $response;
            $response = [];
            foreach ($list as $key => $value) {
                $value = (array) $value;
                $listCat = ['id' => $value['id'], 'name' => trim($value['name'])];
                DB::table('brands')->upsert($listCat, ['id']);
                $response[] = $listCat;
            }

            //DB::table('brands')->upsert($response, ['id']);
            $column = 'name';
            array_multisort(array_column($response, $column), $response);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $response = response()->json(['status' => 'error', 'response' => $exception]);
            } else {
                $response = response()->json(['status' => 'error', 'response' => $e->getMessage()]);
            }
        }
        return $response;
    }

    protected function getExcelProductoUdp(Request $request) {
        set_time_limit(0);
        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $apiAcount = env('API_ACOUNTNAME');
        $apiEnviroment = env('API_ENVIROMENT');

        $file = Null;

        $productIds = $request->arrProduct;
        $campos = $request->campos;
        $productoNames = $request->arrProductNames;

        $client = new Client();



        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];

        $lineas = [];

//        $lineaSpecifications = [];
//        $lineaDimensions = [];
//        $specification2 = [];

        foreach ($productIds as $producto) {
            try {

                $urlApiProductobyContext = $baseUrl . "/api/catalog_system/pvt/products/ProductGet//" . urlencode($producto);
                $request = $client->get($urlApiProductobyContext, [
                    'http_errors' => false,
                    'headers' => $headers
                ]);
                $response = json_decode($request->getBody());
                $response = (array) $response;

//                $brand = DB::table('marcas')
//                                ->select(
//                                        'product_id', 'product_name', 'product_ref_id', 'category_name', 'brand'
//                                )
//                                ->distinct()->get()->toArray();
//
//
////                $response['brand'] = 
////                $response['category'] =      
                $lineas[] = $response;

//                $lineas[]['brand'] = $response;
                $productoActual = (array) $response;

                $productIds[] = $productoActual['Id'];
                $productoNames[] = $productoActual['Name'];
                $productoRefId[] = $productoActual['RefId'];
            } catch (RequestException $e) {
                if ($e->hasResponse()) {
                    $exception = (string) $e->getResponse()->getBody();
                    $lineas[] = ['Id' => $producto, $campos[1] => $exception];
                } else {
                    $lineas[] = ['Id' => $producto, $campos[1] => $e->getMessage()];
                }
            }
        }


        if (count($lineas) > 0) {

            $file = app('App\Http\Controllers\SpreadsheetController')->exportToExcelProductos($lineas, $campos);
            //$file = app('App\Http\Controllers\SpreadsheetController')->addSheetInfoProductosToExcel($file, $this->marcas, $this->categorias);
            $file = app('App\Http\Controllers\SpreadsheetController')->addSheetInfoToExcel($file, [], [], [], [], [], $this->marcas, $this->categorias);
            $file = app('App\Http\Controllers\SpreadsheetController')->addSheetValidationProducts($file);
        }
        return json_encode($file);
    }

    protected function enviarUDP(Request $request) {
        set_time_limit(0);
        $result = [];

        $validated = $request->validate([
            'producto_udp' => 'required|mimes:xlsx,csv,xls|max:32000',
        ]);
        $file1 = $request->file('producto_udp');

        //update productos
        $updaterProducto = app('App\Http\Controllers\SpreadsheetController')->generateJsonProductoUDP($file1);
        foreach ($updaterProducto as $producto) {
            $result[] = $this->updateProductos($producto);
        }

         $archivo = '/log_' . date('m_d_Y_h_i_s_a').'.txt';
        $fp = fopen($_SERVER['DOCUMENT_ROOT'].'/'.$archivo, 'w');
        foreach ($result as $linea) {
            fwrite($fp, 'Status ' . $linea['status'] . ': ProductId ' . $linea['ProductoId'] . ': ' . $linea['mensaje'] . PHP_EOL);
        };
        fclose($fp);
        return json_encode($archivo);
    }

    protected function updateProductos($producto) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');

        $client = new Client();
        $url = $baseUrl . "/api/catalog/pvt/product/";
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];

        $lineas = '';
        try {
            $Id = $producto['Id'];
            $request = $client->put($url . $Id, [
                'http_errors' => true,
                'headers' => $headers,
                'form_params' => $producto
            ]);

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['ProductoId' => $Id, 'status' => 200, 'mensaje' => 'Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['ProductoId' => $Id, 'status' => 304, 'mensaje' => 'Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['ProductoId' => $Id, 'status' => 404, 'mensaje' => 'Producto no encontrado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas = ['ProductoId' => $Id, 'status' => 500, 'mensaje' => $exception];
            } else {
                $lineas = ['ProductoId' => $Id, 'status' => 500, 'mensaje' => $e->getMessage()];
            }
        }

        return $lineas;
    }

}
