<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\DB;

class SpreadsheetController extends BaseController {

    const camposExcluidosdelPrincipal = ['SkuSpecifications', 'ProductSpecifications', 'Images', 'Price', 'Inventory', 'Dimension', 'RealDimension'];
    const camposObjectdelPrincipal = ['Dimension', 'RealDimension'];

    function importToJson($file1) {


        $name = time() . '.xlsx';
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file1);

        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $worksheet = $spreadsheet->getSheet(0); //
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

        $startRow = 2;

        for ($i = 1; $i < $highestColumnIndex + 1; $i++) {
            $valorColumna = $worksheet->getCellByColumnAndRow($i, 1)->getFormattedValue();
            $pa = strpos($valorColumna, '(');
            $pc = strpos($valorColumna, ')');
            if ($pa > 0)
                $valorColumna = substr($valorColumna, 0, $pa - 1);
            $columns[] = trim(str_replace('_', '', $valorColumna));
        }

        ;
        $data_insert = [];
        for ($i = $startRow; $i <= $highestRow; $i++) {
            $id = $worksheet->getCell("A$i")->getValue();
            if (empty($id) || !is_numeric($id))
                continue;

            $data_row = [];
            foreach ($columns as $col => $field) {
                $val = $worksheet->getCellByColumnAndRow($col + 1, $i)->getFormattedValue();
                // $val = $worksheet->getCell("$field")->getValue();
                $data_row[$field] = $val;
            }
            $data_insert[] = $data_row;
        }


        return json_encode(['data' => $data_insert, 'columns' => $columns]);
    }

    public function exportToExcel($lineas, $campos) {

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        // $camposExcluidosdelPrincipal = ['SkuSpecifications', 'ProductSpecifications', 'Images', 'Price', 'Inventory'];

        foreach ($campos as $key => $value) {
            if (in_array($value, self::camposExcluidosdelPrincipal)) {
                unset($campos[$key]);
            }
        }

        foreach ($campos as $campo) {
            if ($campo == 'CategoryId') {
                if (!array_key_exists('CategoryName', $campos))
                    array_push($campos, 'CategoryName');
                //unset($linea['CategoryId']);
            }
            if ($campo == 'BrandId') {
                if (!array_key_exists('BrandName', $campos))
                    array_push($campos, 'BrandName');
                //unset($linea['CategoryId']);
            }
        }

        $sheet->fromArray(
                $campos, // The data to set
                NULL, // Array values with this value will not be set
                'A1'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );
        $rows = 2;

        $nueva = [];
        foreach ($lineas as $linea) {
            $linea2 = [];
            foreach ($campos as $campo) {
                if ($campo == 'Name')
                    $campo = 'SkuName';


                if ($campo == 'CategoryName') {
                    $categoryName = DB::table('categories')->select('name')->Where('id', $linea['CategoryId'])->get()->toArray();
                    $linea['CategoryName'] = $categoryName[0]->name;
                }
                if ($campo == 'BrandName') {
                    $categoryName = DB::table('brands')->select('name')->Where('id', $linea['BrandId'])->get()->toArray();
                    $linea['BrandName'] = $categoryName[0]->name;
                }
                if (array_key_exists($campo, $linea)) {
                    if (is_object($linea[$campo]) || is_array($linea[$campo]))
                        $linea[$campo] = json_encode($linea[$campo], JSON_UNESCAPED_UNICODE);
                    $linea2[] = $linea[$campo];
                }
                //$linea2[] = json_decode(json_encode($linea[$campo]), true);
            }
            $nueva[] = $linea2;
        }
        $sheet->fromArray(
                $nueva, // The data to set
                NULL, // Array values with this value will not be set
                'A2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );

        foreach ($sheet->getColumnIterator() as $column) {
            $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
        }


//        $type = 'xlsx';
        //$filename = tempnam('export','');
        $filename = 'sku_' . date('m_d_Y_h_i_s_a');
//        unlink($filename);
        $fileName = $filename . ".xlsx";
//        if ($type == 'xlsx') {
        $writer = new Xlsx($spreadsheet);
//        } else if ($type == 'xls') {
//            $writer = new Xls($spreadsheet);
//        }
        $writer->save('export/' . $fileName);

        return ('export/' . $fileName);
    }

    public function exportToExcelProductos($lineas, &$campos) {

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

//        foreach ($campos as $key => $value) {
//            if (in_array($value, self::camposExcluidosdelPrincipal)) {
//                unset($campos[$key]);
//            }
//        }


        foreach ($campos as $campo) {
            if ($campo == 'CategoryId') {
                if (!array_key_exists('CategoryName', $campos))
                    array_push($campos, 'CategoryName');
                //unset($linea['CategoryId']);
            }
            if ($campo == 'BrandId') {
                if (!array_key_exists('BrandName', $campos))
                    array_push($campos, 'BrandName');
                //unset($linea['CategoryId']);
            }
        }

        $sheet->fromArray(
                $campos, // The data to set
                NULL, // Array values with this value will not be set
                'A1'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );
        $rows = 2;

        $nueva = [];
        foreach ($lineas as $linea) {
            $linea2 = [];
            foreach ($campos as $campo) {
                if ($campo == 'CategoryName') {
                    if (array_key_exists('CategoryId', $linea)) {
                        $categoryName = DB::table('categories')->select('name')->Where('id', $linea['CategoryId'])->get()->toArray();
                        $linea['CategoryName'] = $categoryName[0]->name;
                    }
//                    if (!array_key_exists('CategoryName', $campos))
//                        array_push($campos, 'CategoryName');
                    //unset($linea['CategoryId']);
                }
                if ($campo == 'BrandName') {
                    if (array_key_exists('BrandId', $linea)) {
                        $categoryName = DB::table('brands')->select('name')->Where('id', $linea['BrandId'])->get()->toArray();
                        $linea['BrandName'] = $categoryName[0]->name;
//                    if (!array_key_exists('BrandName', $campos))
//                    array_push($campos, 'BrandName');
                        //unset($linea['CategoryId']);
                    }
                }
                if (array_key_exists($campo, $linea)) {
//                    if (is_object($linea[$campo]) || is_array($linea[$campo]))
//                        $linea[$campo] = json_encode($linea[$campo], JSON_UNESCAPED_UNICODE);
                    $linea2[] = $linea[$campo] ? $linea[$campo] : "";
                }
            }
            $nueva[] = $linea2;
        }
        $sheet->fromArray(
                $nueva, // The data to set
                NULL, // Array values with this value will not be set
                'A2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );

//        $type = 'xlsx';
        //$filename = tempnam('export','');
        $filename = 'productos_' . date('m_d_Y_h_i_s_a');
//        unlink($filename);
        $fileName = $filename . ".xlsx";
//        if ($type == 'xlsx') {
        $writer = new Xlsx($spreadsheet);
//        } else if ($type == 'xls') {
//            $writer = new Xls($spreadsheet);
//        }
        $writer->save('export/' . $fileName);

        return ('export/' . $fileName);
    }

    public function createWorkSheet($lineas, $campos) {
        
    }

    public function addSheetInfoToExcel($inputFileName, $colores = [], $color = [], $talla = [], $tallaZapato = [], $material = [], $brands = [], $categories = []) {

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $newIndexWorkSheet = $spreadsheet->getSheetCount() + 1;
        $worksheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'Info(No Tocar)');
        $spreadsheet->addSheet($worksheet, $newIndexWorkSheet);
        //$worksheet = $spreadsheet->getSheet($newIndexWorkSheet - 1); //

        $columnas = ['Colores', '', '', 'Color', '', '', 'Talla', '', '', 'Talla Zapato', '', '', 'Material', '', '', 'Brands', '', '', 'Categries',];

        $worksheet->fromArray(
                $columnas, // The data to set
                NULL, // Array values with this value will not be set
                'A1'         // Top left coordinate of the worksheet range where
        );

        $worksheet->fromArray(
                $colores, // The data to set
                NULL, // Array values with this value will not be set
                'A2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );

        $worksheet->fromArray(
                $color, // The data to set
                NULL, // Array values with this value will not be set
                'D2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );

        $worksheet->fromArray(
                $talla, // The data to set
                NULL, // Array values with this value will not be set
                'G2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );
        $worksheet->fromArray(
                $tallaZapato, // The data to set
                NULL, // Array values with this value will not be set
                'J2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );
        $worksheet->fromArray(
                $material, // The data to set
                NULL, // Array values with this value will not be set
                'M2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );
        $worksheet->fromArray(
                $brands, // The data to set
                NULL, // Array values with this value will not be set
                'P2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );

        $worksheet->fromArray(
                $categories, // The data to set
                NULL, // Array values with this value will not be set
                'S2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );

        $worksheet->setSheetState(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::SHEETSTATE_HIDDEN);



        //  $marcas, $categorias




        $writer = new Xlsx($spreadsheet);
        $writer->save($inputFileName);
        return $inputFileName;
    }

    public function addSheetInfoProductosToExcel($inputFileName, $brands, $categories) {

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $newIndexWorkSheet = $spreadsheet->getSheetCount() + 1;
        $worksheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'Info(No Tocar)');
        $spreadsheet->addSheet($worksheet, $newIndexWorkSheet);
        //$worksheet = $spreadsheet->getSheet($newIndexWorkSheet - 1); //

        $columnas = ['Brands', '', '', 'Categories', '', ''];

        $worksheet->fromArray(
                $columnas, // The data to set
                NULL, // Array values with this value will not be set
                'A1'         // Top left coordinate of the worksheet range where
        );

        $worksheet->fromArray(
                $brands, // The data to set
                NULL, // Array values with this value will not be set
                'A2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );

        $worksheet->fromArray(
                $categories, // The data to set
                NULL, // Array values with this value will not be set
                'D2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );

        $worksheet->setSheetState(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::SHEETSTATE_HIDDEN);


        $writer = new Xlsx($spreadsheet);
        $writer->save($inputFileName);
        return $inputFileName;
    }

    public function addSheetToExcel($inputFileName, $lineas, $campos, $nameWorkSheet) {

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $newIndexWorkSheet = $spreadsheet->getSheetCount() + 1;
        $worksheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $nameWorkSheet);
        $spreadsheet->addSheet($worksheet, $newIndexWorkSheet);
        $worksheet = $spreadsheet->getSheet($newIndexWorkSheet - 1); //

        $worksheet->fromArray(
                $campos, // The data to set
                NULL, // Array values with this value will not be set
                'A1'         // Top left coordinate of the worksheet range where
        );
        $rows = 2;


        $nueva = [];
        foreach ($lineas as $linea) {
            $linea2 = [];
            foreach ($campos as $campo) {
                if (($campo == 'Name') && count($linea) <= 3)
                    $campo = 'SkuName';
                if (array_key_exists($campo, $linea)) {
                    if (is_object($linea[$campo]) || is_array($linea[$campo]))
                        $linea[$campo] = json_encode($linea[$campo], JSON_UNESCAPED_UNICODE);
                    $linea2[] = $linea[$campo];
                }
                //$linea2[] = json_decode(json_encode($linea[$campo]), true);
            }
            $nueva[] = $linea2;
        }
        $worksheet->fromArray(
                $nueva, // The data to set
                NULL, // Array values with this value will not be set
                'A2'         // Top left coordinate of the worksheet range where
                //    we want to set these values (default is A1)
        );

        $writer = new Xlsx($spreadsheet);
        $writer->save($inputFileName);

        return $inputFileName;
    }

    public function generateJsonUDP($inputFileName) {

        //  $inputFileName = 'export/sku_10_15_2021_01_22_00_am.xlsx';

        /**  Identify the type of $inputFileName  * */
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        /**  Create a new Reader of the type that has been identified  * */
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        /**  Load $inputFileName to a Spreadsheet Object  * */
        $spreadsheet = $reader->load($inputFileName);


        $sheets = $spreadsheet->getSheetNames();

        $worksheet = $spreadsheet->getSheet(0); //
// Get the highest row and column numbers referenced in the worksheet
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);


        $data = [];
        $dataSS = [];
        $dataPS = [];
        $dataI = [];
        $dataP = [];
        $dataPF = [];

        $keySS = ['Id', 'FieldId', 'FieldValueId', 'Text', 'FieldName'];
        $keyPS = [];
        $keyI = ['SkuFileId', 'label', 'name', 'text', 'url'];

        $keyP = ['itemId', 'listPrice', 'costPric', 'markup', 'basePrice', 'fixedPrices'];

        $keyPF = [];

        $rigaSS = [];
        $rigaPS = [];
        $rigaI = [];
        $rigaP = [];
        $rigaPF = [];




        for ($row = 1; $row <= $highestRow; $row++) {
            $riga = [];


            $specification = [];
            $specificationP = [];
            $specificationI = [];
            $worksheetSS = '';
            for ($col = 1; $col <= $highestColumnIndex; $col++) {
                $valorColumna = $worksheet->getCellByColumnAndRow($col, 1)->getFormattedValue();

                if ($valorColumna == 'SkuSpecifications' && $row != 1) {
                    $worksheetSS = $spreadsheet->getSheetByName('SkuSpecifications'); //
                    $highestRowsSS = $worksheetSS->getHighestRow(); // e.g. 10
                    $highestColumnSS = $worksheetSS->getHighestColumn(); // e.g 'F'
                    $highestColumnIndexSS = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumnSS);

                    $specification = [];
                    if ($worksheetSS) {
                        $worksheetSS->setAutoFilter('A1:E' . $highestRowsSS);

                        //$autoFilter = $spreadsheet->getActiveSheet()->getAutoFilter($spreadsheet->getActiveSheet()->calculateWorksheetDimension());
                        $autoFilter = $worksheetSS->getAutoFilter();
                        $columnFilter = $autoFilter->getColumn('A');
                        $columnFilter->setFilterType(
                                \PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column::AUTOFILTER_FILTERTYPE_FILTER
                        );
                        $columnFilter->createRule()
                                ->setRule(
                                        \PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule::AUTOFILTER_COLUMN_RULE_EQUAL, $worksheet->getCell('A' . ($row))->getValue());
                        $autoFilter = $worksheetSS->getAutoFilter();
                        $autoFilter->showHideRows();
                        $range = $worksheetSS->rangeToArray('A1:E1');
                        $range2 = $worksheetSS->rangeToArray('A' . $row . ':E' . $row);
                        $i = 0;
                        foreach ($worksheetSS->getRowIterator() as $rowSS) {
                            if ($worksheetSS->getRowDimension($rowSS->getRowIndex())->getVisible() == 1) {
                                if ($rowSS->getRowIndex() > 1) {
                                    $idSS = $worksheetSS->getCell('A' . ($rowSS->getRowIndex()))->getValue();
                                    $idPrincipal = $worksheet->getCell('A' . ($row))->getValue();
                                    if ($idSS == $idPrincipal) {
                                        // $fieldName = $worksheetSS->getCell('C' . ($rowSS->getRowIndex()))->getValue();
                                        //$fieldName = $worksheetSS->getCellByColumnAndRow(3, $rowSS->getRowIndex())->getFormattedValue();
                                        //$fieldvalue = $worksheetSS->getCellByColumnAndRow(5, $rowSS->getRowIndex())->getFormattedValue();
                                        $Id = $worksheetSS->getCell('B' . ($rowSS->getRowIndex()))->getValue();
                                        $FieldId = $worksheetSS->getCell('C' . ($rowSS->getRowIndex()))->getValue();
                                        $FieldValueId = $worksheetSS->getCell('D' . ($rowSS->getRowIndex()))->getValue();
                                        $Text = $worksheetSS->getCell('E' . ($rowSS->getRowIndex()))->getValue();
                                        $FieldName = $worksheetSS->getCell('F' . ($rowSS->getRowIndex()))->getValue();
                                        if (\strlen($FieldName) > 0)
                                            $specification[] = ['Id' => $Id, 'FieldId' => $FieldId, 'FieldValueId' => $FieldValueId, 'Text' => $Text, 'FieldName' => $FieldName];
                                    }
                                }
                            }
                        }
                    }
                    $rigaSS = $specification;
                } else
                if ($valorColumna == 'ProductSpecifications' && $row != 1) {
//                    $worksheetPS = $spreadsheet->getSheetByName('ProductSpecifications'); //
//                    $highestRowsPS = $worksheetPS->getHighestRow(); // e.g. 10
//                    $highestColumnPS = $worksheetPS->getHighestColumn(); // e.g 'F'
//                    $highestColumnIndexPS = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumnPS);
//
                    $specification = [];
//                    if ($worksheetSS) {
//                        $worksheetSS->setAutoFilter('A1:E' . $highestRowsPS);
//
//                        //$autoFilter = $spreadsheet->getActiveSheet()->getAutoFilter($spreadsheet->getActiveSheet()->calculateWorksheetDimension());
//                        $autoFilter = $worksheetPS->getAutoFilter();
//                        $columnFilter = $autoFilter->getColumn('A');
//                        $columnFilter->setFilterType(
//                                \PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column::AUTOFILTER_FILTERTYPE_FILTER
//                        );
//                        $columnFilter->createRule()
//                                ->setRule(
//                                        \PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule::AUTOFILTER_COLUMN_RULE_EQUAL, $worksheet->getCell('A' . ($row))->getValue());
//                        $autoFilter = $worksheetPS->getAutoFilter();
//                        $autoFilter->showHideRows();
//                        $range = $worksheetPS->rangeToArray('A1:E1');
//                        $range2 = $worksheetSS->rangeToArray('A' . $row . ':E' . $row);
//                        $i = 0;
//                        foreach ($worksheetSS->getRowIterator() as $rowSS) {
//                            if ($worksheetSS->getRowDimension($rowSS->getRowIndex())->getVisible() == 1) {
//                                if ($rowSS->getRowIndex() > 1) {
//                                    $idSS = $worksheetSS->getCell('A' . ($rowSS->getRowIndex()))->getValue();
//                                    $idPrincipal = $worksheet->getCell('A' . ($row))->getValue();
//                                    if ($idSS == $idPrincipal) {
//                                        // $fieldName = $worksheetSS->getCell('C' . ($rowSS->getRowIndex()))->getValue();
//                                        $fieldName = $worksheetSS->getCellByColumnAndRow(3, $rowSS->getRowIndex())->getFormattedValue();
//                                        $fieldvalue = $worksheetSS->getCellByColumnAndRow(5, $rowSS->getRowIndex())->getFormattedValue();
//
//                                        if (\strlen($fieldvalue) > 0)
//                                            $specification[] = ['fieldName' => $fieldName, 'fieldValue' => $fieldvalue];
//                                    }
//                                }
//                            }
//                        }
//                    }
                    $rigaPS = $specification;
                } else if ($valorColumna == 'Images' && $row != 1) {
                    $worksheetI = $spreadsheet->getSheetByName('Images'); //
                    $highestRowsI = $worksheetI->getHighestRow(); // e.g. 10
                    $highestColumnI = $worksheetI->getHighestColumn(); // e.g 'F'
                    $highestColumnIndexI = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumnI);

                    $specification = [];
                    if ($worksheetI) {
                        $worksheetI->setAutoFilter('A1:G' . $highestRowsI);

                        //$autoFilter = $spreadsheet->getActiveSheet()->getAutoFilter($spreadsheet->getActiveSheet()->calculateWorksheetDimension());
                        $autoFilter = $worksheetI->getAutoFilter();
                        $columnFilter = $autoFilter->getColumn('A');
                        $columnFilter->setFilterType(
                                \PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column::AUTOFILTER_FILTERTYPE_FILTER
                        );
                        $columnFilter->createRule()
                                ->setRule(
                                        \PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule::AUTOFILTER_COLUMN_RULE_EQUAL, $worksheet->getCell('A' . ($row))->getValue());
                        $autoFilter = $worksheetI->getAutoFilter();
                        $autoFilter->showHideRows();
//                        $range = $worksheetI->rangeToArray('A1:G1');
//                        $range2 = $worksheetSS->rangeToArray('A' . $row . ':G' . $row);
                        $i = 0;
                        foreach ($worksheetI->getRowIterator() as $rowI) {
                            if ($worksheetI->getRowDimension($rowI->getRowIndex())->getVisible() == 1) {
                                if ($rowI->getRowIndex() > 1) {
                                    $idI = $worksheetI->getCell('A' . ($rowI->getRowIndex()))->getValue();
                                    $idPrincipal = $worksheet->getCell('A' . ($row))->getValue();
                                    if ($idI == $idPrincipal) {
                                        // $fieldName = $worksheetSS->getCell('C' . ($rowSS->getRowIndex()))->getValue();
                                        $SkuFileId = $worksheetI->getCell('B' . ($rowI->getRowIndex()))->getValue();
                                        $label = $worksheetI->getCell('F' . ($rowI->getRowIndex()))->getValue();
                                        $name = $worksheetI->getCell('D' . ($rowI->getRowIndex()))->getValue();
                                        //$text= 
                                        $url = $worksheetI->getCell('G' . ($rowI->getRowIndex()))->getValue();
                                        if (\strlen($url) > 0)
                                            $specificationI[] = ['SkuFileId' => $SkuFileId, 'label' => $label, 'name' => $name, 'text' => $text, 'url' => $url];
                                    }
                                }
                            }
                        }
                    }
                    $rigaI = $specification;
                } else if ($valorColumna == 'Price' && $row != 1) {
                    $worksheetP = $spreadsheet->getSheetByName('Prices'); //
                    $highestRowsP = $worksheetP->getHighestRow(); // e.g. 10
                    $highestColumnP = $worksheetP->getHighestColumn(); // e.g 'F'
                    $highestColumnIndexP = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumnP);

                    $specification = [];
                    if ($worksheetP) {
                        $worksheetP->setAutoFilter('A1:F' . $highestRowsP);

                        //$autoFilter = $spreadsheet->getActiveSheet()->getAutoFilter($spreadsheet->getActiveSheet()->calculateWorksheetDimension());
                        $autoFilter = $worksheetP->getAutoFilter();
                        $columnFilter = $autoFilter->getColumn('A');
                        $columnFilter->setFilterType(
                                \PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column::AUTOFILTER_FILTERTYPE_FILTER
                        );
                        $columnFilter->createRule()
                                ->setRule(
                                        \PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule::AUTOFILTER_COLUMN_RULE_EQUAL, $worksheet->getCell('A' . ($row))->getValue());
                        $autoFilter = $worksheetP->getAutoFilter();
                        $autoFilter->showHideRows();
//                        $range = $worksheetI->rangeToArray('A1:G1');
//                        $range2 = $worksheetSS->rangeToArray('A' . $row . ':G' . $row);
                        $i = 0;
                        foreach ($worksheetP->getRowIterator() as $rowP) {
                            if ($worksheetP->getRowDimension($rowP->getRowIndex())->getVisible() == 1) {
                                if ($rowP->getRowIndex() > 1) {
                                    $idI = $worksheetP->getCell('A' . ($rowP->getRowIndex()))->getValue();
                                    $idPrincipal = $worksheet->getCell('A' . ($row))->getValue();
                                    if ($idI == $idPrincipal) {

                                        $itemId = $worksheetP->getCell('A' . ($rowP->getRowIndex()))->getValue();
                                        $listPrice = $worksheetP->getCell('B' . ($rowP->getRowIndex()))->getValue();
                                        $costPrice = $worksheetP->getCell('C' . ($rowP->getRowIndex()))->getValue();
                                        $markup = $worksheetP->getCell('D' . ($rowP->getRowIndex()))->getValue();
                                        $basePrice = $worksheetP->getCell('E' . ($rowP->getRowIndex()))->getValue();
                                        $fixedPrices = $worksheetP->getCell('F' . ($rowP->getRowIndex()))->getValue();

                                        if (\strlen($basePrice) > 0)
                                            $specificationP = ['itemId' => $itemId, 'listPrice' => $listPrice, 'costPrice' => $costPrice, 'markup' => $markup, 'basePrice' => $basePrice, 'fixedPrices' => $fixedPrices];
                                    }
                                }
                            }
                        }
                    }
                    $rigaP = $specificationP;
                } else if ($valorColumna == 'PricingFixed' && $row != 1) {
//                    $worksheet2 = $spreadsheet->getSheetByName('Pricing'); //
//                    $range = $worksheet2->rangeToArray('N' . $row . ':P' . $row);
//                    $rigaPF[] = ['Currency' => $range[0][0], 'SalePrice' => $range[0][1], 'CurrencySymbol' => $range[0][2]];
                } else if ($valorColumna == 'Stock' && $row != 1) {
                    $worksheetS = $spreadsheet->getSheetByName('Stock'); //
                    $highestRowsS = $worksheetS->getHighestRow(); // e.g. 10
                    $highestColumnS = $worksheetS->getHighestColumn(); // e.g 'F'
                    $highestColumnIndexS = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumnS);

                    $specification = [];
                    if ($worksheetS) {
                        $worksheetS->setAutoFilter('A1:G' . $highestRowsS);

                        //$autoFilter = $spreadsheet->getActiveSheet()->getAutoFilter($spreadsheet->getActiveSheet()->calculateWorksheetDimension());
                        $autoFilter = $worksheetS->getAutoFilter();
                        $columnFilter = $autoFilter->getColumn('A');
                        $columnFilter->setFilterType(
                                \PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column::AUTOFILTER_FILTERTYPE_FILTER
                        );
                        $columnFilter->createRule()
                                ->setRule(
                                        \PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule::AUTOFILTER_COLUMN_RULE_EQUAL, $worksheet->getCell('A' . ($row))->getValue());
                        $autoFilter = $worksheetS->getAutoFilter();
                        $autoFilter->showHideRows();
//                        $range = $worksheetI->rangeToArray('A1:G1');
//                        $range2 = $worksheetSS->rangeToArray('A' . $row . ':G' . $row);
                        $i = 0;
                        foreach ($worksheetS->getRowIterator() as $rowS) {
                            if ($worksheetS->getRowDimension($rowI->getRowIndex())->getVisible() == 1) {
                                if ($rowS->getRowIndex() > 1) {
                                    $idS = $worksheetS->getCell('A' . ($rowI->getRowIndex()))->getValue();
                                    $idPrincipal = $worksheet->getCell('A' . ($row))->getValue();
                                    if ($idS == $idPrincipal) {
                                        // skuId	warehouseId	warehouseName	totalQuantity	reservedQuantity	hasUnlimitedQuantity

                                        $isUnlimited = $worksheetS->getCell('G' . ($rowS->getRowIndex()))->getValue();
                                        $unlimitedQuantity = $isUnlimited ? true : false;
                                        $quantity = $worksheetS->getCell('D' . ($rowS->getRowIndex()))->getValue();

                                        if (\strlen($url) > 0)
                                            $specificationS[] = ['unlimitedQuantity' => $unlimitedQuantity, 'quantity' => $quantity];
                                    }
                                }
                            }
                        }
                    }
                    $rigaS = $specificationS;
                }else {
                    //$camposExcluidosdelPrincipal = ['SkuSpecifications', 'ProductSpecifications', 'Images', 'Price'];
                    if (!in_array($valorColumna, self::camposExcluidosdelPrincipal)) {
                        $riga[] = $worksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
                    }
                };
            }
            if (1 === $row) {
                // Header row. Save it in "$keys".
                $keys = $riga;

                continue;
            }
            $data = array_combine($keys, $riga);
            $dataSS = $rigaSS;
            $dataPS = $rigaPS;
            $dataI = $rigaI;
            $dataP = $rigaP;
            $dataPF = $rigaPF;

            $dataFinal[] = ['skus' => $data, 'specifications' => $dataSS, 'productSpecifications' => $dataPS, 'images' => $dataI, 'price' => $dataP, 'priceFixed' => $dataPF];
        }

        return json_encode($dataFinal, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    public function generateJsonSkuUDP($inputFileName) {

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getSheet(0); //
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
        $data = [];
        foreach ($worksheet->getRowIterator() as $row) {
            $sku = $worksheet->getCell('A' . ($row->getRowIndex()))->getValue();
            $riga = [];
            for ($col = 1; $col <= $highestColumnIndex; $col++) {
                $valorColumna = $worksheet->getCellByColumnAndRow($col, 1)->getFormattedValue();
                if (in_array($valorColumna, self::camposExcluidosdelPrincipal)) {
                    
                } else {
                    if (in_array($valorColumna, self::camposObjectdelPrincipal)) {

                        if ($valorColumna == 'Dimension') {
                            
                        }
                        if ($valorColumna == 'RealDimension') {
                            
                        }
                    } else {
                        $riga[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
                    }
                }
            }

            $worksheetD = $spreadsheet->getSheetByName('Dimensions'); // 
            if ($worksheetD) {
                $highestRowsD = $worksheetD->getHighestRow(); // e.g. 10
                $highestColumnD = $worksheetD->getHighestColumn(); // e.g 'F'
                $highestColumnIndexD = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumnD);
                $specificationD = [];
                if ($row->getRowIndex() > 1) {
                    $cubicweight = $worksheetD->getCell('D' . ($row->getRowIndex()))->getValue();
                    $height = $worksheetD->getCell('E' . ($row->getRowIndex()))->getValue();
                    $length = $worksheetD->getCell('F' . ($row->getRowIndex()))->getValue();
                    $weight = $worksheetD->getCell('G' . ($row->getRowIndex()))->getValue();
                    $width = $worksheetD->getCell('H' . ($row->getRowIndex()))->getValue();
                    $specificationD = ['CubicWeight' => $cubicweight, 'Height' => $height, 'Length' => $length, 'weightKg' => $weight, 'Width' => $width];
                    $riga[] = $cubicweight;
                    $riga[] = $height;
                    $riga[] = $length;
                    $riga[] = $weight;
                    $riga[] = $width;

                    //$riga[] = json_encode($specificationD);
                } else {
                    $riga[] = 'CubicWeight';
                    $riga[] = 'Height';
                    $riga[] = 'Length';
                    $riga[] = 'weightKg';
                    $riga[] = 'Width';
                    //$riga[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
                }
            }
            //***************************************
            $worksheetRD = $spreadsheet->getSheetByName('RealDimensions'); //
            if ($worksheetRD) {
                $highestRowsRD = $worksheetRD->getHighestRow(); // e.g. 10
                $highestColumnRD = $worksheetRD->getHighestColumn(); // e.g 'F'
                $highestColumnIndexD = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumnD);
                $specificationRD = [];

                if ($row->getRowIndex() > 1) {
                    $cubicweight = $worksheetRD->getCell('D' . ($row->getRowIndex()))->getValue();
                    $height = $worksheetRD->getCell('E' . ($row->getRowIndex()))->getValue();
                    $length = $worksheetRD->getCell('F' . ($row->getRowIndex()))->getValue();
                    $weight = $worksheetRD->getCell('G' . ($row->getRowIndex()))->getValue();
                    $width = $worksheetRD->getCell('H' . ($row->getRowIndex()))->getValue();
                    $specificationRD = ['PackagedCubicWeight' => $cubicweight, 'PackagedHeight' => $height, 'PackagedLength' => $length, 'PackagedWeightKg' => $weight, 'PackagedWidth' => $width];
                    $riga[] = $cubicweight;
                    $riga[] = $height;
                    $riga[] = $length;
                    $riga[] = $weight;
                    $riga[] = $width;

                    //$riga[] = json_encode($specificationD);
                } else {
                    $riga[] = 'PackagedCubicWeight';
                    $riga[] = 'PackagedHeight';
                    $riga[] = 'PackagedLength';
                    $riga[] = 'PackagedWeightKg';
                    $riga[] = 'PackagedWidth';
                    //$riga[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
                }
            }
            if (1 === $row->getRowIndex()) {
                // Header row. Save it in "$keys".
                $keys = $riga;
                continue;
            }
            $data = array_combine($keys, $riga);
            $dataFinal[] = $data;
        }

        return $dataFinal;
    }

    public function generateJsonSkuSpecUDP($inputFileName) {
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $worksheetSS = $spreadsheet->getSheetByName('SkuSpecifications'); //
        $rigaSS = [];
        $specification = [];
        if ($worksheetSS) {
            $highestRowsSS = $worksheetSS->getHighestRow(); // e.g. 10
            $highestColumnSS = $worksheetSS->getHighestColumn(); // e.g 'F'
            $highestColumnIndexSS = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumnSS);
            $i = 0;
            $range = $worksheetSS->rangeToArray('D1:' . $highestColumnSS . '1');
            foreach ($worksheetSS->getRowIterator() as $rowSS) {
                $row = $rowSS->getRowIndex();
                if ($row > 1) {
                    $sku = $worksheetSS->getCell('A' . ($rowSS->getRowIndex()))->getValue();
                    $SkuSpecifications = app('App\Http\Controllers\CatalogosController')->getSkusSpecifications($sku);
                    $range2 = $worksheetSS->rangeToArray('D' . $row . ':' . $highestColumnSS . $row);
                    $j = 0;
                    $specification = [];
                    foreach ($range[0] as $value) {

                        if ($value == 'Color')
                            $FieldId = 41;
                        if ($value == 'Colores')
                            $FieldId = 42;
                        if ($value == 'Talla')
                            $FieldId = 40;
                        if ($value == 'Talla zapatos')
                            $FieldId = 43;
                        if ($value == 'Material')
                            $FieldId = 44;

                        $result = DB::table('specifications')
                                        ->where('Text', $range2[0][$j])
                                        ->where('FieldId', $FieldId)->get();
                        $result = $result->toArray();
                        if (count($result) > 0) {
                            $FieldValueId = $result[0]->FieldValueId;

                            $result = DB::table('temp_sku_specifications')
                                            ->where('FieldId', $FieldId)->get();
                            $result = $result->toArray();
                            if (count($result) > 0)
                                $id = $result[0]->Id ? $result[0]->Id : null;
                            else
                                $id = null;


                            if (\strlen($range2[0][$j]) > 0) {
                                $specification[] = ['id' => $id, 'SkuId' => $sku, 'FieldId' => $FieldId, 'FieldValueId' => $FieldValueId, 'Text' => $range2[0][$j]];
                            }
                        }
                        $j++;
                    }
                    $rigaSS[] = $specification;
                }
            }
        }
        return $rigaSS;
    }

    public function generateJsonSkuImagesUDP($inputFileName) {
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getSheetByName('Images'); //
        $riga = [];
        $specification = [];
        if ($worksheet) {
            $highestRows = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
            $i = 0;
            foreach ($worksheet->getRowIterator() as $row) {
//                $specification = [];
                if ($row->getRowIndex() > 1) {
                    $range = $worksheet->rangeToArray('D' . $row->getRowIndex() . ':' . $highestColumn . $row->getRowIndex());
                    $j = 0;
                    foreach ($range[0] as $value) {
                        $sku = $worksheet->getCell('A' . ($row->getRowIndex()))->getValue();
                        $SkuFileId = '';
                        $name = '';
                        $isMain = false;
                        $label = '';
                        $text = '';
                        $url = $value;
                        if (\strlen($url) > 0)
                            $specification[] = ['skuId' => $sku, 'skuFileId' => $SkuFileId, 'IsMain' => $isMain, 'Label' => $label, 'Name' => $name, 'Text' => $text, 'Url' => $url];
                    }
                }
            }
        }
        return $specification;
    }

    public function generateJsonSkuStockUDP($inputFileName) {
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getSheetByName('Stock'); //
        $riga = [];
        $specification = [];
        if ($worksheet) {
            $highestRows = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

            $i = 0;
            foreach ($worksheet->getRowIterator() as $row) {
                if ($worksheet->getRowDimension($row->getRowIndex())->getVisible() == 1) {
                    if ($row->getRowIndex() > 1) {

                        $skuId = $worksheet->getCell('A' . ($row->getRowIndex()))->getValue();
                        //$warehouseId = $worksheet->getCell('D' . ($row->getRowIndex()))->getValue();
                        $warehouseId = '01';
                        $isUnlimited = $worksheet->getCell('F' . ($row->getRowIndex()))->getValue();
                        $unlimitedQuantity = $isUnlimited ? true : false;
                        $quantity = $worksheet->getCell('D' . ($row->getRowIndex()))->getValue();
                        if (\strlen($quantity) > 0)
                            $specification[] = ['skuId' => $skuId, 'warehouseId' => $warehouseId, 'unlimitedQuantity' => $unlimitedQuantity, 'quantity' => $quantity];
                    }
                }
            }
        }
        return $specification;
    }

    public function generateJsonSkuPriceUDP($inputFileName) {
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getSheetByName('Prices'); //
        $riga = [];
        $specification = [];
        if ($worksheet) {
            $highestRows = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

            $i = 0;
            foreach ($worksheet->getRowIterator() as $row) {
                if ($row->getRowIndex() > 1) {

                    $itemId = $worksheet->getCell('A' . ($row->getRowIndex()))->getValue();
                    $listPrice = $worksheet->getCell('E' . ($row->getRowIndex()))->getValue();
//                    $costPrice = $worksheet->getCell('F' . ($row->getRowIndex()))->getValue();
                    $costPrice = '';
                    //$costPrice = 1;
//                    $markup = $worksheet->getCell('G' . ($row->getRowIndex()))->getValue();
                    $markup = '';
                    $basePrice = $worksheet->getCell('D' . ($row->getRowIndex()))->getValue();
                    // $fixedPrices = $worksheet->getCell('I' . ($row->getRowIndex()))->getValue();
                    $fixedPrices = null;
                    //$markup = (($basePrice - $costPrice) / $costPrice) * 100;

                    $specification[] = ['itemId' => $itemId, 'listPrice' => $listPrice, 'basePrice' => $basePrice];

                    //$specification[] = ['itemId' => $itemId, 'listPrice' => $listPrice, 'costPrice' => $costPrice, 'markup' => $markup, 'basePrice' => $basePrice, 'fixedPrice' => $fixedPrices];
                    // $specification[] = ['itemId' => $itemId, 'listPrice' => $listPrice, 'costPrice' => $costPrice, 'markup' => $markup, 'basePrice' => $basePrice, 'fixedPrices' => $fixedPrices];
                }
            }
        }
        return $specification;
    }

    public function generateJsonSkuDimensionUDP($inputFileName) {
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getSheetByName('Dimensions'); //

        $riga = [];
        $specification = [];
        if ($worksheet) {
            $highestRows = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
            $i = 0;
            foreach ($worksheet->getRowIterator() as $row) {
                if ($row->getRowIndex() > 1) {
                    $cubicweight = $worksheet->getCell('D' . ($row->getRowIndex()))->getValue();
                    $height = $worksheet->getCell('E' . ($row->getRowIndex()))->getValue();
                    $length = $worksheet->getCell('F' . ($row->getRowIndex()))->getValue();
                    $weight = $worksheet->getCell('G' . ($row->getRowIndex()))->getValue();
                    $width = $worksheet->getCell('H' . ($row->getRowIndex()))->getValue();

//                    if (\strlen($basePrice) > 0)
                    $specification[] = ['cubicweight' => $cubicweight, 'height' => $height, 'length' => $length, 'weight' => $weight, 'width' => $width];
                }
            }
        }
        return $specification;
    }

    public function generateJsonProductSpecUDP($inputFileName) {
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getSheetByName('ProductSpecifications'); //
        $rigaSS = [];
        $specification = [];
        if ($worksheet) {
            $i = 0;
            $highestRowsSS = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

            foreach ($worksheet->getRowIterator() as $row) {
                $specification = [];
                if ($rowSS->getRowIndex() > 1) {
                    $sku = $worksheet->getCell('A' . ($row->getRowIndex()))->getValue();
                    $Id = $worksheet->getCell('B' . ($row->getRowIndex()))->getValue();
                    $FieldId = $worksheet->getCell('C' . ($row->getRowIndex()))->getValue();
                    $FieldValueId = $worksheet->getCell('D' . ($row->getRowIndex()))->getValue();
                    $Text = $worksheet->getCell('E' . ($row->getRowIndex()))->getValue();
                    $FieldName = $worksheet->getCell('F' . ($row->getRowIndex()))->getValue();
                    if (\strlen($FieldName) > 0)
                        $specification = ['skuId' => $sku, 'Id' => $Id, 'FieldId' => $FieldId, 'FieldValueId' => $FieldValueId, 'Text' => $Text, 'FieldName' => $FieldName];

                    $riga[] = $specification;
                }
            }
        }
        return $riga;
    }

    public function addSheetValidationSkuSpec($inputFileName, $NameWorkSheet, $campos) {
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getSheetByName($NameWorkSheet); //
        if ($worksheet) {
            $highestRows = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
            $i = 0;

            $workSheetData = $spreadsheet->getSheetByName('Info(No Tocar)');
            //colores 
            //$columnas = [['D'=>'E'], E=>B, F=>H, G=>K, H=>N];
            foreach ($worksheet->getRowIterator() as $row) {
                //Color
                $validation = $worksheet->getCell('D' . ($row->getRowIndex()))->getDataValidation();
                $maxRowList = $workSheetData->getHighestRow('E');
                $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pick from list');
                $validation->setPrompt('Please pick a value from the drop-down list.');
                $validation->setFormula1('=\'Info(No Tocar)\'!$E$2:$E$' . $maxRowList);
                //Colores
                $validation = $worksheet->getCell('E' . ($row->getRowIndex()))->getDataValidation();
                $maxRowList = $workSheetData->getHighestRow('B');
                $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pick from list');
                $validation->setPrompt('Please pick a value from the drop-down list.');
                $validation->setFormula1('=\'Info(No Tocar)\'!$B$2:$B$' . $maxRowList);
                //talla
                $validation = $worksheet->getCell('F' . ($row->getRowIndex()))->getDataValidation();
                $maxRowList = $workSheetData->getHighestRow('H');
                $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pick from list');
                $validation->setPrompt('Please pick a value from the drop-down list.');
                $validation->setFormula1('=\'Info(No Tocar)\'!$H$2:$H$' . $maxRowList);
                //talla zapato
                $validation = $worksheet->getCell('G' . ($row->getRowIndex()))->getDataValidation();
                $maxRowList = $workSheetData->getHighestRow('K');
                $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pick from list');
                $validation->setPrompt('Please pick a value from the drop-down list.');
                $validation->setFormula1('=\'Info(No Tocar)\'!$K$2:$K$' . $maxRowList);
                //Material
                $validation = $worksheet->getCell('H' . ($row->getRowIndex()))->getDataValidation();
                $maxRowList = $workSheetData->getHighestRow('N');
                $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pick from list');
                $validation->setPrompt('Please pick a value from the drop-down list.');
                $validation->setFormula1('=\'Info(No Tocar)\'!$N$2:$N$' . $maxRowList);
            }
            foreach ($worksheet->getColumnIterator() as $column) {
                $worksheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
            }
        }
//        $worksheethide = $spreadsheet->getSheetByName('SkuSpecificationsHide'); //
//        $worksheethide->setSheetState(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::SHEETSTATE_HIDDEN);

        $writer = new Xlsx($spreadsheet);
        $writer->save($inputFileName);
        return $inputFileName;
    }

    public function generateJsonProductoUDP($inputFileName) {

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getSheet(0); //
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
        $data = [];
        foreach ($worksheet->getRowIterator() as $row) {
            $sku = $worksheet->getCell('A' . ($row->getRowIndex()))->getValue();
            $riga = [];
            for ($col = 1; $col <= $highestColumnIndex; $col++) {
                $valorColumna = $worksheet->getCellByColumnAndRow($col, 1)->getFormattedValue();
                if ($valorColumna == 'ProductName' && $row->getRowIndex() < 2) {
                    $riga[] = 'Name';
                } else
                if ($valorColumna == 'CategoryId' && $row->getRowIndex() > 1) {
                    $categoryName = $worksheet->getCellByColumnAndRow($highestColumnIndex - 1, $row->getRowIndex())->getValue();

                    $categoryId = DB::table('categories')->select('id')->Where('name', $categoryName)->get()->toArray();
                    $riga[] = $categoryId[0]->id;
                } else
                if ($valorColumna == 'BrandId' && $row->getRowIndex() > 1) {
                    $brandName = $worksheet->getCell($highestColumn . ($row->getRowIndex()))->getValue();

                    $brandId = DB::table('brands')->select('id')->Where('name', $brandName)->get()->toArray();
                    $riga[] = $brandId[0]->id;
                } else
                    $riga[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
            }

            if (1 === $row->getRowIndex()) {
                // Header row. Save it in "$keys".
                $keys = $riga;
                continue;
            }
            $data = array_combine($keys, $riga);
            $dataFinal[] = $data;
        }

        return $dataFinal;
    }

    public function addSheetValidationProducts($inputFileName) {
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
//        $worksheet = $spreadsheet->getSheetByName($NameWorkSheet); 
        $worksheet = $spreadsheet->getSheet(0); //
        if ($worksheet) {
            $highestRows = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
            $i = 0;

            $workSheetData = $spreadsheet->getSheetByName('Info(No Tocar)');
            //$columnas = [['D'=>'E'], E=>B, F=>H, G=>K, H=>N];
            foreach ($worksheet->getRowIterator() as $row) {
                //Category
                if ($row->getRowIndex() > 1) {
                    $validation = $worksheet->getCellByColumnAndRow($highestColumnIndex - 1, $row->getRowIndex())->getDataValidation();
                    //$validation = $worksheet->getCell('I' . ($row->getRowIndex()))->getDataValidation();
                    $maxRowList = $workSheetData->getHighestRow('T');
                    $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                    $validation->setAllowBlank(false);
                    $validation->setShowInputMessage(true);
                    $validation->setShowErrorMessage(true);
                    $validation->setShowDropDown(true);
                    $validation->setErrorTitle('Input error');
                    $validation->setError('Value is not in list.');
                    $validation->setPromptTitle('Pick from list');
                    $validation->setPrompt('Please pick a value from the drop-down list.');
                    $validation->setFormula1('=\'Info(No Tocar)\'!$T$2:$T$' . $maxRowList);

                    //Brand
                    $validation = $worksheet->getCell($highestColumn . ($row->getRowIndex()))->getDataValidation();
                    $maxRowList = $workSheetData->getHighestRow('Q');
                    $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                    $validation->setAllowBlank(false);
                    $validation->setShowInputMessage(true);
                    $validation->setShowErrorMessage(true);
                    $validation->setShowDropDown(true);
                    $validation->setErrorTitle('Input error');
                    $validation->setError('Value is not in list.');
                    $validation->setPromptTitle('Pick from list');
                    $validation->setPrompt('Please pick a value from the drop-down list.');
                    $validation->setFormula1('=\'Info(No Tocar)\'!$Q$2:$Q$' . $maxRowList);
                }
            }
            foreach ($worksheet->getColumnIterator() as $column) {
                $worksheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);

//
                $ColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($column->getColumnIndex());
//              dd( $worksheet->getCellByColumnAndRow($ColumnIndex, 1)->getValue() );
                if (($worksheet->getCellByColumnAndRow($ColumnIndex, 1)->getValue() == 'CategoryId') ||
                        ($worksheet->getCellByColumnAndRow($ColumnIndex, 1)->getValue() == 'BrandId')) {
                    $worksheet->getColumnDimension($column->getColumnIndex())->setAutoSize(false);
                    $worksheet->getColumnDimension($column->getColumnIndex())->setWidth(0);
                }
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($inputFileName);
        return $inputFileName;
    }

    public function addSheetValidationProductsNew($inputFileName) {
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
//        $worksheet = $spreadsheet->getSheetByName($NameWorkSheet); 
        $worksheet = $spreadsheet->getSheet(0); //
        if ($worksheet) {
            $highestRows = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
            $i = 0;

            $workSheetData = $spreadsheet->getSheetByName('Info(No Tocar)');
            //$columnas = [['D'=>'E'], E=>B, F=>H, G=>K, H=>N];
            foreach ($worksheet->getRowIterator() as $row) {

                if ($row->getRowIndex() > 1) {
                    //Category
                    $validation = $worksheet->getCellByColumnAndRow(2, $row->getRowIndex())->getDataValidation();
                    //$validation = $worksheet->getCell('I' . ($row->getRowIndex()))->getDataValidation();
                    $maxRowList = $workSheetData->getHighestRow('T');
                    $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                    $validation->setAllowBlank(false);
                    $validation->setShowInputMessage(true);
                    $validation->setShowErrorMessage(true);
                    $validation->setShowDropDown(true);
                    $validation->setErrorTitle('Input error');
                    $validation->setError('Value is not in list.');
                    $validation->setPromptTitle('Pick from list');
                    $validation->setPrompt('Please pick a value from the drop-down list.');
                    $validation->setFormula1('=\'Info(No Tocar)\'!$T$2:$T$' . $maxRowList);

                    //Brand
                    $validation = $worksheet->getCellByColumnAndRow(3, $row->getRowIndex())->getDataValidation();
                    $maxRowList = $workSheetData->getHighestRow('Q');
                    $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                    $validation->setAllowBlank(false);
                    $validation->setShowInputMessage(true);
                    $validation->setShowErrorMessage(true);
                    $validation->setShowDropDown(true);
                    $validation->setErrorTitle('Input error');
                    $validation->setError('Value is not in list.');
                    $validation->setPromptTitle('Pick from list');
                    $validation->setPrompt('Please pick a value from the drop-down list.');
                    $validation->setFormula1('=\'Info(No Tocar)\'!$Q$2:$Q$' . $maxRowList);

                    //color
                    $validation = $worksheet->getCellByColumnAndRow(7, $row->getRowIndex())->getDataValidation();
                    $maxRowList = $workSheetData->getHighestRow('E');
                    $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                    $validation->setAllowBlank(false);
                    $validation->setShowInputMessage(true);
                    $validation->setShowErrorMessage(true);
                    $validation->setShowDropDown(true);
                    $validation->setErrorTitle('Input error');
                    $validation->setError('Value is not in list.');
                    $validation->setPromptTitle('Pick from list');
                    $validation->setPrompt('Please pick a value from the drop-down list.');
                    $validation->setFormula1('=\'Info(No Tocar)\'!$E$2:$E$' . $maxRowList);

                    //colores
                    $validation = $worksheet->getCellByColumnAndRow(8, $row->getRowIndex())->getDataValidation();
                    $maxRowList = $workSheetData->getHighestRow('B');
                    $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                    $validation->setAllowBlank(false);
                    $validation->setShowInputMessage(true);
                    $validation->setShowErrorMessage(true);
                    $validation->setShowDropDown(true);
                    $validation->setErrorTitle('Input error');
                    $validation->setError('Value is not in list.');
                    $validation->setPromptTitle('Pick from list');
                    $validation->setPrompt('Please pick a value from the drop-down list.');
                    $validation->setFormula1('=\'Info(No Tocar)\'!$B$2:$B$' . $maxRowList);

                    //Talle
                    $validation = $worksheet->getCellByColumnAndRow(9, $row->getRowIndex())->getDataValidation();
                    $maxRowList = $workSheetData->getHighestRow('H');
                    $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                    $validation->setAllowBlank(false);
                    $validation->setShowInputMessage(true);
                    $validation->setShowErrorMessage(true);
                    $validation->setShowDropDown(true);
                    $validation->setErrorTitle('Input error');
                    $validation->setError('Value is not in list.');
                    $validation->setPromptTitle('Pick from list');
                    $validation->setPrompt('Please pick a value from the drop-down list.');
                    $validation->setFormula1('=\'Info(No Tocar)\'!$H$2:$H$' . $maxRowList);

                    //Talla zapato
                    $validation = $worksheet->getCellByColumnAndRow(10, $row->getRowIndex())->getDataValidation();
                    $maxRowList = $workSheetData->getHighestRow('K');
                    $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                    $validation->setAllowBlank(false);
                    $validation->setShowInputMessage(true);
                    $validation->setShowErrorMessage(true);
                    $validation->setShowDropDown(true);
                    $validation->setErrorTitle('Input error');
                    $validation->setError('Value is not in list.');
                    $validation->setPromptTitle('Pick from list');
                    $validation->setPrompt('Please pick a value from the drop-down list.');
                    $validation->setFormula1('=\'Info(No Tocar)\'!$K$2:$K$' . $maxRowList);

                    //Talla zapato
                    $validation = $worksheet->getCellByColumnAndRow(11, $row->getRowIndex())->getDataValidation();
                    $maxRowList = $workSheetData->getHighestRow('N');
                    $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                    $validation->setAllowBlank(false);
                    $validation->setShowInputMessage(true);
                    $validation->setShowErrorMessage(true);
                    $validation->setShowDropDown(true);
                    $validation->setErrorTitle('Input error');
                    $validation->setError('Value is not in list.');
                    $validation->setPromptTitle('Pick from list');
                    $validation->setPrompt('Please pick a value from the drop-down list.');
                    $validation->setFormula1('=\'Info(No Tocar)\'!$N$2:$N$' . $maxRowList);
                }
            }
            foreach ($worksheet->getColumnIterator() as $column) {
                $worksheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);

//
                $ColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($column->getColumnIndex());
//              dd( $worksheet->getCellByColumnAndRow($ColumnIndex, 1)->getValue() );
                if (($worksheet->getCellByColumnAndRow($ColumnIndex, 1)->getValue() == 'CategoryId') ||
                        ($worksheet->getCellByColumnAndRow($ColumnIndex, 1)->getValue() == 'BrandId')) {
                    $worksheet->getColumnDimension($column->getColumnIndex())->setAutoSize(false);
                    $worksheet->getColumnDimension($column->getColumnIndex())->setWidth(0);
                }
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($inputFileName);
        return $inputFileName;
    }

//    public function generateJsonSkuNew($inputFileName) {
//
//        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
//        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
//        $spreadsheet = $reader->load($inputFileName);
//        $worksheet = $spreadsheet->getSheet(0); //
//        $highestRow = $worksheet->getHighestRow(); // e.g. 10
//        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
//        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
//        $data = [];
//        foreach ($worksheet->getRowIterator() as $row) {
//            $sku = $worksheet->getCell('B' . ($row->getRowIndex()))->getValue();
//            $riga = [];
//            for ($col = 1; $col <= $highestColumnIndex; $col++) {
//                $valorColumna = $worksheet->getCellByColumnAndRow($col, 1)->getFormattedValue();
//                if (in_array($valorColumna, self::camposExcluidosdelPrincipal)) {
//                    
//                } else {
//                    if (in_array($valorColumna, self::camposObjectdelPrincipal)) {
//
//                        if ($valorColumna == 'Dimension') {
//                            
//                        }
//                        if ($valorColumna == 'RealDimension') {
//                            
//                        }
//                    } else {
//                        $riga[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
//                    }
//                }
//            }
//
//            $worksheetD = $spreadsheet->getSheetByName('Dimensions'); // 
//            if ($worksheetD) {
//                $highestRowsD = $worksheetD->getHighestRow(); // e.g. 10
//                $highestColumnD = $worksheetD->getHighestColumn(); // e.g 'F'
//                $highestColumnIndexD = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumnD);
//                $specificationD = [];
//                if ($row->getRowIndex() > 1) {
//                    $cubicweight = $worksheetD->getCell('D' . ($row->getRowIndex()))->getValue();
//                    $height = $worksheetD->getCell('E' . ($row->getRowIndex()))->getValue();
//                    $length = $worksheetD->getCell('F' . ($row->getRowIndex()))->getValue();
//                    $weight = $worksheetD->getCell('G' . ($row->getRowIndex()))->getValue();
//                    $width = $worksheetD->getCell('H' . ($row->getRowIndex()))->getValue();
//                    $specificationD = ['CubicWeight' => $cubicweight, 'Height' => $height, 'Length' => $length, 'weightKg' => $weight, 'Width' => $width];
//                    $riga[] = $cubicweight;
//                    $riga[] = $height;
//                    $riga[] = $length;
//                    $riga[] = $weight;
//                    $riga[] = $width;
//
//                    //$riga[] = json_encode($specificationD);
//                } else {
//                    $riga[] = 'CubicWeight';
//                    $riga[] = 'Height';
//                    $riga[] = 'Length';
//                    $riga[] = 'weightKg';
//                    $riga[] = 'Width';
//                    //$riga[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
//                }
//            }
//            //***************************************
//            $worksheetRD = $spreadsheet->getSheetByName('RealDimensions'); //
//            if ($worksheetRD) {
//                $highestRowsRD = $worksheetRD->getHighestRow(); // e.g. 10
//                $highestColumnRD = $worksheetRD->getHighestColumn(); // e.g 'F'
//                $highestColumnIndexD = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumnD);
//                $specificationRD = [];
//
//                if ($row->getRowIndex() > 1) {
//                    $cubicweight = $worksheetRD->getCell('D' . ($row->getRowIndex()))->getValue();
//                    $height = $worksheetRD->getCell('E' . ($row->getRowIndex()))->getValue();
//                    $length = $worksheetRD->getCell('F' . ($row->getRowIndex()))->getValue();
//                    $weight = $worksheetRD->getCell('G' . ($row->getRowIndex()))->getValue();
//                    $width = $worksheetRD->getCell('H' . ($row->getRowIndex()))->getValue();
//                    $specificationRD = ['PackagedCubicWeight' => $cubicweight, 'PackagedHeight' => $height, 'PackagedLength' => $length, 'PackagedWeightKg' => $weight, 'PackagedWidth' => $width];
//                    $riga[] = $cubicweight;
//                    $riga[] = $height;
//                    $riga[] = $length;
//                    $riga[] = $weight;
//                    $riga[] = $width;
//
//                    //$riga[] = json_encode($specificationD);
//                } else {
//                    $riga[] = 'PackagedCubicWeight';
//                    $riga[] = 'PackagedHeight';
//                    $riga[] = 'PackagedLength';
//                    $riga[] = 'PackagedWeightKg';
//                    $riga[] = 'PackagedWidth';
//                    //$riga[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
//                }
//            }
//            if (1 === $row->getRowIndex()) {
//                // Header row. Save it in "$keys".
//                $keys = $riga;
//                continue;
//            }
//            $data = array_combine($keys, $riga);
//            $dataFinal[] = $data;
//        }
//
//        return $dataFinal;
//    }

    public function generateJsonProductoNew($inputFileName) {

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getSheet(0); //
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
        $data = [];
        foreach ($worksheet->getRowIterator() as $row) {
//            $product = $worksheet->getCell('A' . ($row->getRowIndex()))->getValue();
            $rigaP = [];
            for ($col = 2; $col <= $highestColumnIndex; $col++) {
                $valorColumna = $worksheet->getCellByColumnAndRow($col, 1)->getFormattedValue();
                if ($row->getRowIndex() == 1) {
                    if ($valorColumna == 'CategoryName') {  //PRODUCTOS
                        $rigaP[] = 'CategoryId';
                    } else
                    if ($valorColumna == 'BrandName') {
                        $rigaP[] = 'BrandId';
                    } else
                    if ($valorColumna == 'Product Reference Code') {
                        $rigaP[] = 'RefId';
                    } else
                    if ($valorColumna == 'ProductName') {
                        $rigaP[] = 'Name';
                    } else
                    if ($valorColumna == 'Product Description') {
                        $rigaP[] = 'Description';
                    } else
                    if ($valorColumna == 'MetaTag Description') {
                        $rigaP[] = 'MetaTagDescription';     //PRODUCTOS
                    }
                } else {
                    if ($row->getRowIndex() > 1) {

                        if ($valorColumna == 'CategoryName') {

                            $categoryName = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();

                            $categoryId = DB::table('categories')->select('id')->Where('name', $categoryName)->get()->toArray();
                            $rigaP[] = $categoryId[0]->id;
                        } else
                        if ($valorColumna == 'BrandName') {
                            $brandName = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();

                            $brandId = DB::table('brands')->select('id')->Where('name', $brandName)->get()->toArray();
                            $rigaP[] = $brandId[0]->id;
                        } else
                        if (in_array($valorColumna, ['Product Reference Code', 'ProductName', 'Product Description', 'MetaTag Description',])) {
                            $rigaP[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
                        }
                    }
                }
            }

            if (1 === $row->getRowIndex()) {
                // Header row. Save it in "$keys".
                $keysP = $rigaP;
                continue;
            }
            $dataP = array_combine($keysP, $rigaP);
            $dataFinal['Producto'][] = $dataP;
        }

        return $dataFinal;
    }

    public function generateJsonSkuNew($inputFileName) {

        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);
        $worksheet = $spreadsheet->getSheet(0); //
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
        $data = [];
        foreach ($worksheet->getRowIterator() as $row) {
//            $product = $worksheet->getCell('A' . ($row->getRowIndex()))->getValue();
            $rigaP = [];
            $rigaSpec = [];
            $rigaPrice = [];
            $rigaStock = [];
            $rigaImg = [];
            for ($col = 2; $col <= $highestColumnIndex; $col++) {
                $valorColumna = $worksheet->getCellByColumnAndRow($col, 1)->getFormattedValue();
                if ($row->getRowIndex() == 1) {
                    if ($valorColumna == 'SkuName') {  //PRODUCTOS
                        $rigaP[] = 'Name';
                    } else
                    if ($valorColumna == 'Sku EAN/GTIN') {
                        $rigaP[] = 'ean';
                    } else
                    if ($valorColumna == 'SKU Reference Code') {
                        $rigaP[] = 'RefId';
                    } else
                    if ($valorColumna == 'Product Reference Code') {
                        $rigaP[] = 'ProductRefId';
                    } else
                    if (in_array($valorColumna, ['PVP'])) {
                        $rigaPrice[] = 'basePrice'; //$worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
                    } else
                    if ($valorColumna=='Available Quantity') {
                        $rigaStock[] = 'quantity';
                        //$rigaStock[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
                    }

                } else {
                    if ($row->getRowIndex() > 1) {
                        if (in_array($valorColumna, ['SkuName', 'Sku EAN/GTIN', 'SKU Reference Code', 'Product Reference Code'])) {
                            $rigaP[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
                        } else
                        if (in_array($valorColumna, ['PVP'])) {
                            $rigaPrice[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
                        } else
                        if (in_array($valorColumna, ['Available Quantity'])) {
                            $rigaStock[] = $worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getFormattedValue();
                        }
                    }
                }
            }

            if (1 === $row->getRowIndex()) {

                $rigaP[] = 'CubicWeight';
                $rigaP[] = 'Height';
                $rigaP[] = 'Length';
                $rigaP[] = 'weightKg';
                $rigaP[] = 'Width';
                // Header row. Save it in "$keys".
                $keysP = $rigaP;

//                $rigaSpec[] = 'SkuId';
//                $keysSpec = $rigaSpec;

                $rigaPrice[] = 'itemId';
                $keysPrice = $rigaPrice;

                $rigaStock[] = 'skuId';
                $keysStock = $rigaStock;

                continue;
            } else {
                $rigaP[] = '1';
                $rigaP[] = '1';
                $rigaP[] = '1';
                $rigaP[] = '1';
                $rigaP[] = '1';

//                $rigaSpec[] = '';
                $rigaPrice[] = '';
                $rigaStock[] = '';


                $range = $worksheet->rangeToArray('S' . $row->getRowIndex() . ':' . $highestColumn . $row->getRowIndex());
                $j = 0;
                foreach ($range[0] as $value) {
                    $sku = '';
                    $SkuFileId = '';
                    $name = '';
                    $isMain = false;
                    $label = '';
                    $text = '';
                    $url = $value;
                    if (\strlen($url) > 0)
                        $rigaImg[] = ['skuId' => $sku, 'skuFileId' => $SkuFileId, 'IsMain' => $isMain, 'Label' => $label, 'Name' => $name, 'Text' => $text, 'Url' => $url];
                }
                
                $range2 = $worksheet->rangeToArray('G1' . ':' . 'K1');
                $range3 = $worksheet->rangeToArray('G' . $row->getRowIndex() . ':' . 'K' . $row->getRowIndex());
                    $j = 0;
                    $specification = [];
                    foreach ($range2[0] as $value) {

                        if ($value == 'Color')
                            $FieldId = 41;
                        if ($value == 'Colores')
                            $FieldId = 42;
                        if ($value == 'Talla')
                            $FieldId = 40;
                        if ($value == 'Talla zapatos')
                            $FieldId = 43;
                        if ($value == 'Material')
                            $FieldId = 44;

                        $result = DB::table('specifications')
                                        ->where('Text', $range3[0][$j])
                                        ->where('FieldId', $FieldId)->get();
                        $result = $result->toArray();
                        if (count($result) > 0) {
                            $FieldValueId = $result[0]->FieldValueId;

//                            $result = DB::table('temp_sku_specifications')
//                                            ->where('FieldId', $FieldId)->get();
//                            $result = $result->toArray();
//                            if (count($result) > 0)
//                                $id = $result[0]->Id ? $result[0]->Id : null;
//                            else
                                $id = null;


                            if (\strlen($range3[0][$j]) > 0) {
                                $rigaSpec[] = ['id' => $id, 'SkuId' => '', 'FieldId' => $FieldId, 'FieldValueId' => $FieldValueId, 'Text' => $range3[0][$j]];
                            }
                        }
                        $j++;
                    }
                    
                    
            }

            $dataP = array_combine($keysP, $rigaP);
            //$dataSpec = array_combine($keysSpec, $rigaSpec);
            $dataPrice = array_combine($keysPrice, $rigaPrice);
            $dataStock = array_combine($keysStock, $rigaStock);
            $dataImg = $rigaImg;
            $dataSpec=$rigaSpec;

            $dataFinal['Sku'][] = $dataP;
            $dataFinal['SkuSpec'][] = $dataSpec;
            $dataFinal['SkuPrice'][] = $dataPrice;
            $dataFinal['SkuStock'][] = $dataStock;
            $dataFinal['SkuImg'][] = $dataImg;
        }

        return $dataFinal;
    }

}
