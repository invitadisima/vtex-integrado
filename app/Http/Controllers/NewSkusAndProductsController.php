<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\SpreadsheetController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;
use App\Models\Skus;

class NewSkusAndProductsController extends Controller {

//estructura del campo 
//primer valor es como se llama en el api, s
//egundo valor es el nombre para mostrar en excel y el el listado, 
//terecer valor indica si es editable. 
//cuarto valor mostrar si o no
    var $campos = [
        ['CategoryName', 'Category', 0, 1],
        ['BrandName', 'Brand', 0, 1],
        ['RefId', 'ProductRefId', 1, 1],
        ['ProductName', 'ProductName', 0, 1],
        ['SkuName', 'SkuName', 0, 1],
        ['SkuSpecifications', 'SkuSpecifications', 1, 1],
        ['Price', 'Price Base', 1, 1],
        ['Inventory', 'Stock', 1, 1],
        ['Description', 'ProductDescription', 1, 1],
        ['DescriptionShort', 'ProductDescriptionShort', 1, 1],
        ['Title', 'ProductTitle', 1, 1],
        ['MetaTagDescription', 'ProductMetaTagDescription', 1, 1],
        ['Dimension', 'Dimension', 1, 1],
        ['RealDimension', 'RealDimension', 1, 1],
        ['Images', 'Images', 1, 1],
        ['SkuId', 'SkuId', 0, 1], //se llama Id en el api, se muestra en la lista como Id, es editable y se muestra 
        ['ProductId', 'ProductId', 0, 1], //se llama Id en el api, se muestra en la lista como Id, es editable y se muestra 
//        ['IsActive', 'IsActive', 1, 1],
        ['ProductIsActive', 'ProductIsActive', 1, 1],
        ['ProductIsVisible', 'ProductIsVisible', 1, 1],
    ];
    public static $colores;
    public static $color;
    public static $talla;
    public static $tallaZapato;
    public static $material;
    // public $categorias;
    // public $marcas;
    public $specifications;

    public function __construct() {
        $this->categorias = $this->getCategorias();
        $this->marcas = $this->getMarcas();
        $this->specifications = $this->getSpecifications(1);
    }

    //
    public function index() {
        $categorias = [];
        $marcas = [];
        return view('admin.new_skus_productos')->with(['campos' => $this->campos, 'categorias' => $this->categorias, 'marcas' => $this->marcas, 'specifications' => $this->specifications]);
    }

    function import(Request $request) {
        $validated = $request->validate([
            'catalogo_file' => 'required|mimes:xlsx,csv,xls|max:32000',
        ]);
        $file1 = $request->file('catalogo_file');

        //Descomentar estas lineas si la importacion viene de un excel
        $result = app('App\Http\Controllers\SpreadsheetController')->importToJson($file1);
        $result = json_decode($result, true);

        //Descomentar estas lineas si la importacion viene de un array precargado
//          $result['data'] = [
//            ["SkuId" => 539, 'ProductId' => 183, 'SkuName' => 'Vestido midi con escote corazón y flocado floral ROJOS 42'],
//            ["SkuId" => 540, "ProductId" => 185, "SkuName" => "Vestido de cóctel negro con escote bardot encaje y transparecias - Penelope NEGRO  36"],
//            ["SkuId" => 541, "ProductId" => 185, "SkuName" => "Vestido de cóctel negro con escote bardot encaje y transparecias - Penelope NEGRO  38"],
//            ["SkuId" => 542, "ProductId" => 185, "SkuName" => "Vestido de cóctel negro con escote bardot encaje y transparecias - Penelope NEGRO  40"],
//            ["SkuId" => 543, "ProductId" => 185, "SkuName" => "Vestido de cóctel negro con escote bardot encaje y transparecias - Penelope NEGRO  42"],
//        ];
//          $result['columns']= ['SkuId','ProductId','SkuName'];



        return view('admin.catalogo')->with(['data' => $result['data'], 'columns' => $result['columns'], 'campos' => $this->campos]);
    }

    public function getSkusSpecifications($sku) {
        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $client = new Client();
        $skuSpecResponse = [];
        $urlApiSkuSpecifications = $baseUrl . "/api/catalog/pvt/stockkeepingunit/" . urlencode($sku) . "/specification";
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        try {
            $request = $client->get($urlApiSkuSpecifications, [
                'http_errors' => false,
                'headers' => $headers
            ]);
            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                DB::table('temp_sku_specifications')->truncate();
                if ($response) {
                    foreach ($response as $skuSpecResponse) {
                        $skuSpecResponse = (array) $skuSpecResponse;
                        if ($skuSpecResponse['FieldValueId'])
                            DB::table('temp_sku_specifications')
                                    ->Insert(
                                            $skuSpecResponse
                            );
                    }
                }
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas[] = ['Id' => $sku['skuId'], 'mensaje' => $exception];
            } else {
                $lineas[] = ['Id' => $sku['skuId'], 'mensaje' => $e->getMessage()];
            }
        }
        return $skuSpecResponse;
    }

    protected function getExcelSkuProductNew(Request $request) {

        $lineas = [
            ['No' => '1',
                'CategoryName' => '',
                'BrandName' => '',
                'Product Reference Code' => '',
                'ProductName' => '',
                'SkuName' => '',
                'Color' => '',
                'Colores' => '',
                'Talla' => '',
                'Talla zapatos' => '',
                'Material' => '',
                'PVP' => '',
                'Selling Price' => '',
                'Available Quantity' => '',
                'Sku EAN/GTIN' => '',
                'SKU Reference Code' => '',
                'Product Description' => '',
                'MetaTag Description' => '',
                'Image URL 1' => '',
                'Image URL 2' => '',
                'Image URL 3' => '',
                'Image URL 4' => '',
                'Image URL 5' => ''],
            ['No' => '2',
                'CategoryName' => '',
                'BrandName' => '',
                'Product Reference Code' => '',
                'ProductName' => '',
                'SkuName' => '',
                'Color' => '',
                'Colores' => '',
                'Talla' => '',
                'Talla zapatos' => '',
                'Material' => '',
                'PVP' => '',
                'Selling Price' => '',
                'Available Quantity' => '',
                'Sku EAN/GTIN' => '',
                'SKU Reference Code' => '',
                'Product Description' => '',
                'MetaTag Description' => '',
                'Image URL 1' => '',
                'Image URL 2' => '',
                'Image URL 3' => '',
                'Image URL 4' => '',
                'Image URL 5' => ''],
            ['No' => '3',
                'CategoryName' => '',
                'BrandName' => '',
                'Product Reference Code' => '',
                'ProductName' => '',
                'SkuName' => '',
                'Color' => '',
                'Colores' => '',
                'Talla' => '',
                'Talla zapatos' => '',
                'Material' => '',
                'PVP' => '',
                'Selling Price' => '',
                'Available Quantity' => '',
                'Sku EAN/GTIN' => '',
                'SKU Reference Code' => '',
                'Product Description' => '',
                'MetaTag Description' => '',
                'Image URL 1' => '',
                'Image URL 2' => '',
                'Image URL 3' => '',
                'Image URL 4' => '',
                'Image URL 5' => ''],
            ['No' => '4',
                'CategoryName' => '',
                'BrandName' => '',
                'Product Reference Code' => '',
                'ProductName' => '',
                'SkuName' => '',
                'Color' => '',
                'Colores' => '',
                'Talla' => '',
                'Talla zapatos' => '',
                'Material' => '',
                'PVP' => '',
                'Selling Price' => '',
                'Available Quantity' => '',
                'Sku EAN/GTIN' => '',
                'SKU Reference Code' => '',
                'Product Description' => '',
                'MetaTag Description' => '',
                'Image URL 1' => '',
                'Image URL 2' => '',
                'Image URL 3' => '',
                'Image URL 4' => '',
                'Image URL 5' => ''],
            ['No' => '5',
                'CategoryName' => '',
                'BrandName' => '',
                'Product Reference Code' => '',
                'ProductName' => '',
                'SkuName' => '',
                'Color' => '',
                'Colores' => '',
                'Talla' => '',
                'Talla zapatos' => '',
                'Material' => '',
                'PVP' => '',
                'Selling Price' => '',
                'Available Quantity' => '',
                'Sku EAN/GTIN' => '',
                'SKU Reference Code' => '',
                'Product Description' => '',
                'MetaTag Description' => '',
                'Image URL 1' => '',
                'Image URL 2' => '',
                'Image URL 3' => '',
                'Image URL 4' => '',
                'Image URL 5' => '']
        ];
        $campos = ['No', 'CategoryName', 'BrandName', 'Product Reference Code', 'ProductName', 'SkuName', 'Color', 'Colores', 'Talla', 'Talla zapatos', 'Material', 'PVP', 'Selling Price', 'Available Quantity', 'Sku EAN/GTIN', 'SKU Reference Code', 'Product Description', 'MetaTag Description', 'Image URL 1', 'Image URL 2', 'Image URL 3', 'Image URL 4', 'Image URL 5'];
        $file = app('App\Http\Controllers\SpreadsheetController')->exportToExcelProductos($lineas, $campos);
        $file = app('App\Http\Controllers\SpreadsheetController')->addSheetInfoToExcel($file, $this::$colores, $this::$color, $this::$talla, $this::$tallaZapato, $this::$material, $this->marcas, $this->categorias);
        $file = app('App\Http\Controllers\SpreadsheetController')->addSheetValidationProductsNew($file);

//        if (in_array('SkuSpecifications', $campos)) {
//            $skuSpeCampos = ["SkuId", "ProductId", "SkuName", "Color", "Colores", "Talla", "Talla zapatos", "Material",];
//            $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaSpecifications, $skuSpeCampos, 'SkuSpecifications');
//            $file = app('App\Http\Controllers\SpreadsheetController')->addSheetValidationSkuSpec($file, 'SkuSpecifications', ["Color", "Colores", "Talla", "Talla zapatos", "Material"]);
//        }
//        if (in_array('Images', $campos)) {
//
//            $skuImgCampos = ["SkuId", "ProductId", "SkuName", "Image URL 1", "Image URL 2", "Image URL 3", "Image URL 4", "Image URL 5",];
//            $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaImages, $skuImgCampos, 'Images');
//        }
//        if (in_array('Price', $campos)) {
//            $skuPriceCampos = ["SkuId", "ProductId", "SkuName", "basePrice",];
//            $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaPrice, $skuPriceCampos, 'Prices');
//        }
//        if (in_array('Inventory', $campos)) {
//            // $skuStockCampos = ["skuId", "ProductId", "SkuName", "warehouseId", "totalQuantity", "isUnlimited", /* "reservedQuantity", "availableQuantity",  "dockId","salesChannel", "deliveryChannels", "timeToRefill", "dateOfSupplyUtc", "supplyLotId", "keepSellingAfterExpiration", "transfer" */];
//            $skuStockCampos = ["skuId", "ProductId", "SkuName", "totalQuantity"];
//            $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaStock, $skuStockCampos, 'Stock');
//        }
//
//        if (in_array('Dimension', $campos)) {
//            $skuDimensionCampos = ["SkuId", "ProductId", "SkuName", "cubicweight", "height", "length", "weight", "width"];
//            $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaDimensions, $skuDimensionCampos, 'Dimensions');
//        }
//        if (in_array('RealDimension', $campos)) {
//            $skuRealDimensionCampos = ["SkuId", "ProductId", "SkuName", "realCubicWeight", "realHeight", "realLength", "realWeight", "realWidth"];
//            $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaRealDimensions, $skuRealDimensionCampos, 'RealDimensions');
//        }

        return json_encode($file);
    }

    protected function enviarUDP(Request $request) {
        set_time_limit(0);
        $result = [];

        $validated = $request->validate([
            'catalogo_udp' => 'required|mimes:xlsx,csv,xls|max:90000',
        ]);
        $file1 = $request->file('catalogo_udp');


        //create productos
        $updaterProducto = app('App\Http\Controllers\SpreadsheetController')->generateJsonProductoNew($file1);
        $productos = $this->unique_multidim_array($updaterProducto['Producto'], 'RefId');
        foreach ($productos as $producto) {
            $linea = $this->createProductos($producto);

            $result[] = $linea;
        }

        // create skus
        $arraysku = [];
        $updaterSku = app('App\Http\Controllers\SpreadsheetController')->generateJsonSkuNew($file1);
        //$skus = $this->unique_multidim_array($updaterSku['Sku'], 'RefId');

        $i = 0;
        foreach ($updaterSku['Sku'] as $sku) {
            $resultado = $this->createSkus($sku);
            $result[] = $resultado;
            if ($resultado['status'] == 200) {
                
                $updaterSku['SkuPrice'][$i]['itemId'] = $resultado['SkuId'];
                $updaterSku['SkuStock'][$i]['skuId'] = $resultado['SkuId'];
                
                $cuantos = count($updaterSku['SkuImg'][$i]);
                for ($j = 0; $j <= $cuantos - 1; $j++) {
                    $updaterSku['SkuImg'][$i][$j]['skuId'] = $resultado['SkuId'];
                }
                
                 $cuantos = count($updaterSku['SkuSpec'][$i]);
                for ($j = 0; $j <= $cuantos - 1; $j++) {
                    $updaterSku['SkuSpec'][$i][$j]['SkuId'] = $resultado['SkuId'];
                }
            }
            $i++;
        }

        foreach ($updaterSku['SkuImg'] as $sku) {
            foreach ($sku as $img) {
                $result[] = $this->updateSkusImages($img);
            }
        }

        //update Stock
        foreach ($updaterSku['SkuStock'] as $sku) {
            $result[] = $this->updateSkuStock($sku);
        }

        //update price
        foreach ($updaterSku['SkuPrice'] as $sku) {
            $result[] = $this->updateSkuPrice($sku);
        }

        //update skuspecifications
      
        
         foreach ($updaterSku['SkuSpec'] as $sku) {
            foreach ($sku as $spec) {
                $result[] = $this->updateSkusSpecifications($spec);
            }
        }

        $archivo = 'log_' . date('m_d_Y_h_i_s_a').'.txt';
        $fp = fopen($_SERVER['DOCUMENT_ROOT'].'/'.$archivo, 'w');
        foreach ($result as $linea) {
            if (is_array($linea)) {
                if (array_key_exists('SkuId', $linea))
                    fwrite($fp, 'Status ' . $linea['status'] . ': Sku ' . $linea['SkuId'] . ': ' . $linea['mensaje'] . PHP_EOL);
                else
                    fwrite($fp, 'Status ' . $linea['status'] . ': Product ' . $linea['ProductoId'] . ': ' . $linea['mensaje'] . PHP_EOL);
            }
        };
        fclose($fp);
        return json_encode($archivo);
    }

    protected function updateSkus($sku) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');

        $client = new Client();
        $url = $baseUrl . "/api/catalog/pvt/stockkeepingunit/";
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];

        $lineas = '';
        try {
            $Id = $sku['SkuId'];
            $request = $client->put($url . $Id, [
                'http_errors' => false,
                'headers' => $headers,
                'form_params' => $sku
            ]);

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['SkuId' => $Id, 'status' => 200, 'mensaje' => 'Sku: Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['SkuId' => $Id, 'status' => 304, 'mensaje' => 'Sku: Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['SkuId' => $Id, 'status' => 404, 'mensaje' => 'Sku no encontrado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas = ['SkuId' => $Id, 'status' => 500, 'mensaje' => $exception];
            } else {
                $lineas = ['SkuId' => $Id, 'status' => 500, 'mensaje' => $e->getMessage()];
            }
        }

        return $lineas;
    }

    protected function updateSkusImages($sku) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $skuId = $sku['skuId'];

        $client = new Client();

        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        $lineas = [];
        try {
            if ($sku['skuFileId'] <> '') {
                $url = $baseUrl . "/api/catalog/pvt/stockkeepingunit/" . $sku['skuId'] . "/file/" . $sku['skuFileId'];
                unset($sku['skuId']);
                $request = $client->put($url, [
                    'http_errors' => false,
                    'headers' => $headers,
                    'body' => json_encode($sku)
                ]);
            } else {
                $url = $baseUrl . "/api/catalog/pvt/stockkeepingunit/" . $sku['skuId'] . "/file";
                unset($sku['skuFileId']);
                unset($sku['skuId']);
                $request = $client->post($url, [
                    'http_errors' => false,
                    'headers' => $headers,
                    'body' => json_encode($sku)
                ]);
            }

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['SkuId' => $skuId, 'status' => 200, 'mensaje' => 'Image: Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['SkuId' => $skuId, 'status' => 304, 'mensaje' => 'Image: Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['SkuId' => $skuId, 'status' => 404, 'mensaje' => 'Image: Sku no encontrado'];
            } elseif ($statuscode == 500) {
                $lineas = ['SkuId' => $skuId, 'status' => 404, 'mensaje' => 'Image: Error de Servidor'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas[] = ['Id' => $skuId, 'mensaje' => $exception];
            } else {
                $lineas[] = ['Id' => $skuId, 'mensaje' => $e->getMessage()];
            }
        }
        return $lineas;
    }

    protected function updateSkusSpecifications($sku) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');

        $client = new Client();
        $url = $baseUrl . "/api/catalog/pvt/stockkeepingunit/" . $sku['SkuId'] . "/specification";
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        $lineas = [];
        try {
//            if ($sku['id']) {
//                $request = $client->put($url, [
//                    'http_errors' => true,
//                    'headers' => $headers,
//                    'form_params' => $sku
//                ]);
//            } else {
                $request = $client->post($url, [
                    'http_errors' => true,
                    'headers' => $headers,
                    'form_params' => $sku
                ]);
//            }

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['SkuId' => $sku['SkuId'], 'status' => 200, 'mensaje' => 'Specification: Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['SkuId' => $sku['SkuId'], 'status' => 304, 'mensaje' => 'Specification:  Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['SkuId' => $sku['SkuId'], 'status' => 404, 'mensaje' => 'Specification:  Sku no encontrado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas[] = ['Id' => $sku['SkuId'], 'mensaje' => $exception];
            } else {
                $lineas[] = ['Id' => $sku['SkuId'], 'mensaje' => $e->getMessage()];
            }
        }
        return $lineas;
    }

    protected function updateSkuStock($sku) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');

        $client = new Client();
//        /api/logistics/pvt/inventory/skus/skuId/warehouses/warehouseId
        $sku['warehouseId'] = '01';
        $url = $baseUrl . "/api/logistics/pvt/inventory/skus/" . $sku['skuId'] . "/warehouses/" . $sku['warehouseId'];
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        $lineas = [];
        try {
            $request = $client->put($url, [
                'http_errors' => false,
                'headers' => $headers,
                'form_params' => $sku
            ]);

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['SkuId' => $sku['skuId'], 'status' => 200, 'mensaje' => 'Stock: Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['SkuId' => $sku['skuId'], 'status' => 304, 'mensaje' => 'Stock: Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['SkuId' => $sku['skuId'], 'status' => 404, 'mensaje' => 'Stock: Sku no encontrado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas[] = ['Id' => $sku['skuId'], 'mensaje' => $exception];
            } else {
                $lineas[] = ['Id' => $sku['skuId'], 'mensaje' => $e->getMessage()];
            }
        }
        return $lineas;
    }

    protected function updateSkuPrice($sku) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $apiAcount = env('API_ACOUNTNAME');
        $apiEnviroment = env('API_ENVIROMENT');

        $client = new Client();

        $url = "https://api." . $apiEnviroment . "/" . $apiAcount . "/pricing/prices/" . urlencode($sku['itemId']);
        $skuId = $sku['itemId'];
        $sku['listPrice']=floatval($sku['basePrice']);
        $sku['basePrice']=floatval($sku['basePrice']);

        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/vnd.vtex.pricing.v3+json',
            "Content-Type" => "application/json"
        ];

        try {
//            dd($sku);
            unset($sku['itemId']);
            $request = $client->put($url, [
                'http_errors' => true,
                'headers' => $headers,
                'body' => json_encode($sku)
            ]);

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['SkuId' => $skuId, 'status' => 200, 'mensaje' => 'Price: Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['SkuId' => $skuId, 'status' => 304, 'mensaje' => 'Price: Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['SkuId' => $skuId, 'status' => 404, 'mensaje' => 'Price: Sku no encontrado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas[] = ['Id' => $skuId, 'mensaje' => $exception];
            } else {
                $lineas[] = ['Id' => $skuId, 'mensaje' => $e->getMessage()];
            }
        } //catch (ClientException $e) {
//            $lineas[] = Psr7\Message::toString($e->getRequest());
//            $lineas[] = Psr7\Message::toString($e->getResponse());
//        }
        return $lineas;
    }

    public static function getSpecifications($categoryId) {
        $baseUrl = env('API_ENDPOINT');
        $apiAcount = env('API_ACOUNTNAME');
        $apiEnviroment = env('API_ENVIROMENT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $specifications = [];
        $client = new Client();
        $url = $baseUrl . "/api/catalog_system/pub/specification/field/listTreeByCategoryId/" . $categoryId;
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        try {
            $request = $client->get($url, [
                'http_errors' => true,
                'headers' => $headers
            ]);
            $response = json_decode($request->getBody());

            foreach ($response as $fieldId) {
                $fieldId = (array) $fieldId;
                if ($fieldId['IsActive']) {

                    $url = $baseUrl . "/api/catalog_system/pub/specification/fieldvalue/" . $fieldId['FieldId'];


                    $request = $client->get($url, [
                        'http_errors' => true,
                        'headers' => $headers
                    ]);
                    $response2 = json_decode($request->getBody());

                    foreach ($response2 as $fieldValue) {
                        $fieldValue = (array) $fieldValue;
                        if ($fieldValue['IsActive'] == true) {
                            $specifications[] = ['FieldId' => $fieldId['FieldId'], 'Name' => $fieldId['Name'], 'FieldValueId' => $fieldValue['FieldValueId'], 'Text' => $fieldValue['Value']];
                            if ($fieldId['Name'] == 'Colores') {
                                self::$colores[] = [$fieldValue['FieldValueId'], $fieldValue['Value']];
                            }
                            if ($fieldId['Name'] == 'Color') {
                                self::$color[] = [$fieldValue['FieldValueId'], $fieldValue['Value']];
                            }
                            if ($fieldId['Name'] == 'Talla') {
                                self::$talla[] = [$fieldValue['FieldValueId'], $fieldValue['Value']];
                            }
                            if ($fieldId['Name'] == 'Talla Zapato') {
                                self::$tallaZapato[] = [$fieldValue['FieldValueId'], $fieldValue['Value']];
                            }
                            if ($fieldId['Name'] == 'Material') {
                                self::$material[] = [$fieldValue['FieldValueId'], $fieldValue['Value']];
                            }
                        }
                    }
                }
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $response = response()->json(['status' => 'error', 'response' => $exception]);
            } else {
                $response = response()->json(['status' => 'error', 'response' => $e->getMessage()]);
            }
        }

        DB::table('specifications')->truncate();

        DB::table('specifications')
                ->Insert(
                        $specifications
        );

        return $specifications;
    }

    function importToDB(Request $request) {

        if (request()->ajax()) {
            $validated = $request->validate([
                'catalogo_file' => 'required|mimes:xlsx,csv,xls|max:50000',
            ]);
            $file1 = $request->file('catalogo_file');

            try {
                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file1);
                $worksheet = $spreadsheet->getActiveSheet();
                $row_limit = $worksheet->getHighestDataRow();
                $column_limit = $worksheet->getHighestDataColumn();
                $row_range = range(2, $row_limit);



//        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
//        $worksheet = $spreadsheet->getSheet(0); //
//        $highestRow = $worksheet->getHighestRow(); // e.g. 10
//        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
//        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
//
//        
                $column_range = range('A', $column_limit);
                $startcount = 2;
                $data = array();
                foreach ($row_range as $row) {
                    $data[] = [
                        'skuid' => $worksheet->getCell('A' . $row)->getValue(),
                        'sku_name' => $worksheet->getCell('B' . $row)->getValue(),
                        'is_active' => $worksheet->getCell('D' . $row)->getValue(),
                        'ean' => $worksheet->getCell('E' . $row)->getValue(),
                        'product_id' => $worksheet->getCell('T' . $row)->getValue(),
                        'product_name' => $worksheet->getCell('U' . $row)->getValue(),
                        'product_ref_id' => $worksheet->getCell('X' . $row)->getValue(),
                        'departament_id' => $worksheet->getCell('AI' . $row)->getValue(),
                        'departament_name' => $worksheet->getCell('AJ' . $row)->getValue(),
                        'category_id' => $worksheet->getCell('AK' . $row)->getValue(),
                        'category_name' => $worksheet->getCell('AL' . $row)->getValue(),
                        'brand_id' => $worksheet->getCell('AM' . $row)->getValue(),
                        'brand' => $worksheet->getCell('AN' . $row)->getValue(),
                    ];
                    $startcount++;
                }
                DB::table('skus')->upsert($data, ['skuid']);

                $this->categorias = $this->getCategorias();
                $this->marcas = $this->getMarcas();
                $this->specifications = $this->getSpecifications(1);
            } catch (Exception $e) {
                $error_code = $e->errorInfo[1];
                return response()->json(['status' => 'error', 'response' => $error_code]);
            }
            return response()->json(['status' => 'ok', 'response' => 'Importacion Realizada Correctamente']);
        }

//          $result['columns']= ['SkuId','ProductId','SkuName'];

        return view('admin.importdb');
    }

    public static function getCategorias() {
        $baseUrl = env('API_ENDPOINT');
        $apiAcount = env('API_ACOUNTNAME');
        $apiEnviroment = env('API_ENVIROMENT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $client = new Client();
        $url = $baseUrl . "/api/catalog_system/pub/category/tree/8";

        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
        ];
        try {
            $request = $client->get($url, [
                'http_errors' => true,
                'headers' => $headers
            ]);
            $response = json_decode($request->getBody());

            //$response = self::_getTree($response);
            $list = [];
            $list = self::_getListCategory($response, '', $list, 0);
            $listCat = [];
            $response = [];
            foreach ($list as $key => $value) {
                $listCat = ['id' => $value['id'], 'name' => trim($value['name']), 'ruta' => trim($value['ruta'])];
                DB::table('categories')->upsert($listCat, ['id']);
                $response[] = $listCat;
            }
            $column = 'ruta';
            array_multisort(array_column($response, $column), $response);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $response = response()->json(['status' => 'error', 'response' => $exception]);
            } else {
                $response = response()->json(['status' => 'error', 'response' => $e->getMessage()]);
            }
        }

        return $response;
    }

    public static function _getTree($tree) {
        $i = 0;
        $arbol = [];
        foreach ($tree as $hijo) {
//         $arbol[$i]['id'] = $hijo->id;
            $arbol[$i]['id'] = $hijo->id;
            $arbol[$i]['name'] = $hijo->name;
            if ($hijo->hasChildren) {
                $arbol[$i]['nodes'] = self::_getTree($hijo->children);
            }
//            else
//                $arbol[$i]['nodes'] = [];
            $i++;
        }
        return $arbol;
    }

    public static function _getListCategory($tree, $ruta, &$list) {

        foreach ($tree as $hijo) {
            $id = $hijo->id;
            $text = $ruta . $hijo->name;
//            $list.put([$id=>$text]);
            array_push($list, ['id' => $id, 'ruta' => $text, 'name' => $hijo->name]);
            if ($hijo->hasChildren) {
                self::_getListCategory($hijo->children, $text . '/', $list);
            }
        }
        return $list;
    }

    public static function getMarcas() {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $client = new Client();
        $url = $baseUrl . "/api/catalog_system/pvt/brand/list";
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        try {
            $request = $client->get($url, [
                'http_errors' => true,
                'headers' => $headers
            ]);
            $response = json_decode($request->getBody());
            $list = (array) $response;
            $response = [];
            foreach ($list as $key => $value) {
                $value = (array) $value;
                $listCat = ['id' => $value['id'], 'name' => trim($value['name'])];
                DB::table('brands')->upsert($listCat, ['id']);
                $response[] = $listCat;
            }

            //DB::table('brands')->upsert($response, ['id']);
            $column = 'name';
            array_multisort(array_column($response, $column), $response);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $response = response()->json(['status' => 'error', 'response' => $exception]);
            } else {
                $response = response()->json(['status' => 'error', 'response' => $e->getMessage()]);
            }
        }
        return $response;
    }

    protected function updateProductos($producto) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');

        $client = new Client();
        $url = $baseUrl . "/api/catalog/pvt/product/";
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];

        $lineas = '';
        try {
            $Id = $producto['ProductId'];
            $request = $client->put($url . $Id, [
                'http_errors' => true,
                'headers' => $headers,
                'form_params' => $producto
            ]);

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['ProductoId' => $Id, 'status' => 200, 'mensaje' => 'Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['ProductoId' => $Id, 'status' => 304, 'mensaje' => 'Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['ProductoId' => $Id, 'status' => 404, 'mensaje' => 'Producto no encontrado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas = ['ProductoId' => $Id, 'status' => 500, 'mensaje' => $exception];
            } else {
                $lineas = ['ProductoId' => $Id, 'status' => 500, 'mensaje' => $e->getMessage()];
            }
        }

        return $lineas;
    }

    protected function createProductos($producto) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');

        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ];

        $client = new Client();
        $url = $baseUrl . '/api/catalog_system/pvt/products/productgetbyrefid/' . $producto['RefId'];

        $lineas = '';
        try {


            $request = $client->get($url, [
                'http_errors' => true,
                'headers' => $headers,
            ]);
            $statuscode = $request->getStatusCode();

            $headers = [
                "X-VTEX-API-AppKey" => $vtexKey,
                "X-VTEX-API-AppToken" => $vtexToken,
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ];

            if ($statuscode == 200) {
                $productId = json_decode($request->getBody());
                if ($productId) {
                    $productId = $productId->Id;
                    $url = $baseUrl . "/api/catalog/pvt/product/" . $productId;
                    $request = $client->put($url, [
                        'http_errors' => true,
                        'headers' => $headers,
                        'form_params' => $producto
                    ]);
                } else {

                    $url = $baseUrl . "/api/catalog/pvt/product/";
                    $request = $client->post($url, [
                        'http_errors' => true,
                        'headers' => $headers,
                        'form_params' => $producto
                    ]);
                }
            } else {

                $url = $baseUrl . "/api/catalog/pvt/product/";
                $request = $client->post($url, [
                    'http_errors' => false,
                    'headers' => $headers,
                    'form_params' => $producto
                ]);
            }

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['ProductoId' => $producto['RefId'], 'status' => 200, 'mensaje' => 'Creado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['ProductoId' => $producto['RefId'], 'status' => 304, 'mensaje' => 'Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['ProductoId' => $producto['RefId'], 'status' => 404, 'mensaje' => 'Producto no encontrado'];
            } elseif ($statuscode == 409) {
                $lineas = ['ProductoId' => $producto['RefId'], 'status' => 304, 'mensaje' => 'Recurso duplicado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas = ['ProductoId' => $producto['RefId'], 'status' => 500, 'mensaje' => $exception];
            } else {
                $lineas = ['ProductoId' => $producto['RefId'], 'status' => 500, 'mensaje' => $e->getMessage()];
            }
        }

        return $lineas;
    }

    function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    protected function createSkus($producto) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $productId = '';

        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ];
        $client = new Client();
        $url = $baseUrl . '/api/catalog_system/pvt/products/productgetbyrefid/' . $producto['ProductRefId'];
        $lineas = '';
        try {
            $request = $client->get($url, [
                'http_errors' => false,
                'headers' => $headers,
            ]);
            $statuscode = $request->getStatusCode();

            $headers = [
                "X-VTEX-API-AppKey" => $vtexKey,
                "X-VTEX-API-AppToken" => $vtexToken,
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ];
            if ($statuscode == 200) {
                $productId = json_decode($request->getBody());
                if ($productId) {
                    $productId = $productId->Id;
                    // array_push($producto,'ProductId' );
                    $producto['ProductId'] = $productId;
                }
            }


            $url = $baseUrl . "/api/catalog/pvt/stockkeepingunit/";
            $headers = [
                "X-VTEX-API-AppKey" => $vtexKey,
                "X-VTEX-API-AppToken" => $vtexToken,
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ];

            $request = $client->post($url, [
                'http_errors' => false,
                'headers' => $headers,
                'form_params' => $producto
            ]);
            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['SkuId' => $response->Id, 'status' => 200, 'mensaje' => 'Creado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['ProductoId' => $producto['RefId'], 'status' => 304, 'mensaje' => 'Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['ProductoId' => $producto['RefId'], 'status' => 404, 'mensaje' => 'Producto no encontrado'];
            } elseif ($statuscode == 409) {
                $lineas = ['ProductoId' => $producto['RefId'], 'status' => 304, 'mensaje' => 'Recurso duplicado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas = ['ProductoId' => $producto['RefId'], 'status' => 500, 'mensaje' => $exception];
            } else {
                $lineas = ['ProductoId' => $producto['RefId'], 'status' => 500, 'mensaje' => $e->getMessage()];
            }
        }

        return $lineas;
    }

}
