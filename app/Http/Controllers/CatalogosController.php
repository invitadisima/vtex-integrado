<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\SpreadsheetController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;
use App\Models\Skus;

class CatalogosController extends Controller {

//estructura del campo 
//primer valor es como se llama en el api, s
//egundo valor es el nombre para mostrar en excel y el el listado, 
//terecer valor indica si es editable. 
//cuarto valor mostrar si o no
    var $campos = [
        ['Id', 'SkuId', 0, 1], //se llama Id en el api, se muestra en la lista como Id, es editable y se muestra 
        ['ProductId', 'ProductId', 0, 1],
        ['Name', 'SkuName', 0, 1],
        ['ProductRefId', 'ProductRefId', 0, 1],
//        ['ComplementName', 'ComplementName', 1, 0],
        ['ProductName', 'ProductName', 1, 1],
//        ['ProductDescription', 'ProductDescription', 1, 1],
        ['IsActive', 'IsActive', 1, 1],
        ['Dimension', 'Dimension', 1, 1],
//        'Dimension.cubicweight',
//        'Dimension.height',
//        'Dimension.length',
//        'Dimension.weight',
//        'Dimension.width',
        ['RealDimension', 'RealDimension', 1, 1],
//        'RealDimension.realCubicWeight',
//        'RealDimension.realHeight',
//        'RealDimension.realLength',
//        'RealDimension.realWeight',
//        'RealDimension.realWidth',
//        'ManufacturerCode',
        ['Images', 'Images', 1, 1],
//        'Images.ImageUrl',
//        'Images.ImageName',
        ['SkuSpecifications', 'SkuSpecifications', 1, 1],
//        'SkuSpecifications.FieldId',
//        'SkuSpecifications.FieldName',
//        'SkuSpecifications.FieldValueIds',
//        'SkuSpecifications.FieldValues',
//        'SkuSpecifications.isFilter',
//        
//        ['SkuSellers', 'SkuSellers', 1, 1],
//            [
//               {
//              "SellerId": "1",
//              "StockKeepingUnitId": 2001773,
//              "SellerStockKeepingUnitId": "2001773",
//              "IsActive": true,
//              "FreightCommissionPercentage": 0,
//             "ProductCommissionPercentage": 0
//            }
//            ]
        ['Price', 'Price Base', 1, 1],
//        ['Fixed Price', 'Fixed Price', 1, 1],
        ['Inventory', 'Stock', 1, 1],
        ['ActivateIfPossible', 'ActivateIfPossible', 1, 1],
//"PackagedHeight":10
//"PackagedLength":10
//"PackagedWidth":10
//"PackagedWeightKg":10
//"Height":10
//"Length":10
//"Width":10
//"WeightKg":10
//"CubicWeight":0.1667
//
//        ['IsKit', 'IsKit', 1, 1],
//        ['RewardValue', 'RewardValue', 1, 1],
//        ['ManufacturerCode', 'ManufacturerCode', 1, 1],
//        ['CommercialConditionId', 'CommercialConditionId', 1, 1],
//        ['MeasurementUnit', 'MeasurementUnit', 1, 1],
//        ['UnitMultiplier', 'UnitMultiplier', 1, 1],
//        ['ModalType', 'ModalType', 1, 1],
//        ['KitItensSellApart', 'KitItensSellApart', 1, 1],
//        ['Videos', 'Videos', 1, 1],
    ];
    public static $colores;
    public static $color;
    public static $talla;
    public static $tallaZapato;
    public static $material;
    // public $categorias;
    // public $marcas;
    public $specifications;

    public function __construct() {
        $this->categorias = $this->getCategorias();
        $this->marcas = $this->getMarcas();
        $this->specifications = $this->getSpecifications(1);
    }

    //
    public function index() {
        // $this->categorias = json_encode($this->getCategorias(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        // $this->marcas = $this->getMarcas();
        //  dd($categorias);
        $categorias = [];
        $marcas = [];

        $productos = DB::table('skus')
                        ->select(
                                'skuid', 'sku_name', 'product_ref_id', 'product_name', 'category_id', 'category_name', 'brand_id', 'brand', 'departament_id', 'departament_name'
                        )
                        ->distinct()->get()->toArray();
        return view('admin.udp_catalogo')->with(['campos' => $this->campos, 'categorias' => $this->categorias, 'marcas' => $this->marcas, 'productos' => $productos]);
    }

    function import(Request $request) {
        $validated = $request->validate([
            'catalogo_file' => 'required|mimes:xlsx,csv,xls|max:32000',
        ]);
        $file1 = $request->file('catalogo_file');

        //Descomentar estas lineas si la importacion viene de un excel
        $result = app('App\Http\Controllers\SpreadsheetController')->importToJson($file1);
        $result = json_decode($result, true);

        //Descomentar estas lineas si la importacion viene de un array precargado
//          $result['data'] = [
//            ["SkuId" => 539, 'ProductId' => 183, 'SkuName' => 'Vestido midi con escote corazón y flocado floral ROJOS 42'],
//            ["SkuId" => 540, "ProductId" => 185, "SkuName" => "Vestido de cóctel negro con escote bardot encaje y transparecias - Penelope NEGRO  36"],
//            ["SkuId" => 541, "ProductId" => 185, "SkuName" => "Vestido de cóctel negro con escote bardot encaje y transparecias - Penelope NEGRO  38"],
//            ["SkuId" => 542, "ProductId" => 185, "SkuName" => "Vestido de cóctel negro con escote bardot encaje y transparecias - Penelope NEGRO  40"],
//            ["SkuId" => 543, "ProductId" => 185, "SkuName" => "Vestido de cóctel negro con escote bardot encaje y transparecias - Penelope NEGRO  42"],
//        ];
//          $result['columns']= ['SkuId','ProductId','SkuName'];



        return view('admin.catalogo')->with(['data' => $result['data'], 'columns' => $result['columns'], 'campos' => $this->campos]);
    }

    public function getSkusSpecifications($sku) {
        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $client = new Client();
        $skuSpecResponse = [];
        $urlApiSkuSpecifications = $baseUrl . "/api/catalog/pvt/stockkeepingunit/" . urlencode($sku) . "/specification";
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        try {
            $request = $client->get($urlApiSkuSpecifications, [
                'http_errors' => false,
                'headers' => $headers
            ]);
            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                DB::table('temp_sku_specifications')->truncate();
                if ($response) {
                    foreach ($response as $skuSpecResponse) {
                        $skuSpecResponse = (array) $skuSpecResponse;
                        if ($skuSpecResponse['FieldValueId'])
                            DB::table('temp_sku_specifications')
                                    ->Insert(
                                            $skuSpecResponse
                            );
                    }
                }
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas[] = ['Id' => $sku['skuId'], 'mensaje' => $exception];
            } else {
                $lineas[] = ['Id' => $sku['skuId'], 'mensaje' => $e->getMessage()];
            }
        }
        return $skuSpecResponse;
    }

    protected function getExcelSkuCataloGoUdp(Request $request) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $apiAcount = env('API_ACOUNTNAME');
        $apiEnviroment = env('API_ENVIROMENT');

        $file = Null;

        $skuIds = $request->arrSku;
        $campos = $request->campos;
        $productIds = $request->arrProductIds;
        $skuNames = $request->arrSkuNames;

        $client = new Client();


        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];

        $lineas = [];

        $lineaSpecifications = [];
        $lineaDimensions = [];
        $specification2 = [];

        set_time_limit(0);

        foreach ($skuIds as $sku) {
            try {

                $urlApiSkubyContext = $baseUrl . "/api/catalog_system/pvt/sku/stockkeepingunitbyid/" . urlencode($sku);
                $request = $client->get($urlApiSkubyContext, [
                    'http_errors' => false,
                    'headers' => $headers
                ]);
                $response = json_decode($request->getBody());
                $response = (array) $response;
                $lineas[] = $response;
                $skuActual = (array) $response;
                $productIds[] = $skuActual['ProductId'];
                $skuNames[] = $skuActual['SkuName'];


                if (in_array('SkuSpecifications', $campos)) {
                    if ($skuActual['SkuSpecifications'])
                        $SpecificationsSku = (array) $skuActual['SkuSpecifications'];
                    $urlApiSkuSpecifications = $baseUrl . "/api/catalog/pvt/stockkeepingunit/" . urlencode($sku) . "/specification";
                    $request = $client->get($urlApiSkuSpecifications, [
                        'http_errors' => false,
                        'headers' => $headers
                    ]);
                    $response = json_decode($request->getBody());
                    $skuSpecResponse = (array) $response;
                    $i = 0;
                    $specification2[$sku]['SkuId'] = $skuActual['Id'];
                    $specification2[$sku]['ProductId'] = $skuActual['ProductId'];
                    $specification2[$sku]['SkuName'] = $skuActual['SkuName'];
                    $specification2[$sku]['Talla'] = '';
                    $specification2[$sku]['Color'] = '';
                    $specification2[$sku]['Colores'] = '';
                    $specification2[$sku]['Talla Zapato'] = '';
                    $specification2[$sku]['Material'] = '';


//                    
                    foreach ($skuSpecResponse as $specification) {
                        $specification = (array) $specification;
                        $lineaSpecificationHide[] = $specification;


//40	Talla
//41	Color
//42	Colores
//43	Talla Zapato
//35	Color NO
//36	Talla NO
//44	Material
                        $valorTalla = ($specification['FieldId'] == 40) ? $specification['Text'] : '';
                        if ($specification2[$sku]['Talla'] == '')
                            $specification2[$sku]['Talla'] = $valorTalla;

                        $valorColor = ($specification['FieldId'] == 41) ? $specification['Text'] : '';
                        if ($specification2[$sku]['Color'] == '')
                            $specification2[$sku]['Color'] = $valorColor;

                        $valorColores = ($specification['FieldId'] == 42) ? $specification['Text'] : '';
                        if ($specification2[$sku]['Colores'] == '')
                            $specification2[$sku]['Colores'] = $valorColores;

                        $valorTallaZapato = ($specification['FieldId'] == 43) ? $specification['Text'] : '';
                        if ($specification2[$sku]['Talla Zapato'] == '')
                            $specification2[$sku]['Talla Zapato'] = $valorTallaZapato;

                        $valorMaterial = ($specification['FieldId'] == 44) ? $specification['Text'] : '';
                        if ($specification2[$sku]['Material'] == '')
                            $specification2[$sku]['Material'] = $valorMaterial;

                        $lineaSpecifications = $specification2;
                    }



                    // DB::raw("CREATE TEMPORARY TABLE tbl_temp(id VARCHAR(100),SkuId VARCHAR(100),FieldId VARCHAR(100),FieldValueId VARCHAR(100),Text VARCHAR(100)") ;
                }

                if (in_array('Price', $campos)) {
                    $urlApiSkuPrice = "https://api." . $apiEnviroment . "/" . $apiAcount . "/pricing/prices/" . urlencode($sku);
                    $request = $client->get($urlApiSkuPrice, [
                        'http_errors' => false,
                        'headers' => $headers
                    ]);
                    $response = json_decode($request->getBody());
                    $priceResponse = (array) $response;

                    $priceResponse['SkuId'] = $skuActual['Id'];
                    $priceResponse['ProductId'] = $skuActual['ProductId'];
                    $priceResponse['SkuName'] = $skuActual['SkuName'];

                    $i = 0;

                    $lineaPrice[] = $priceResponse;
                }

                if (in_array('Dimension', $campos)) {
                    if ($skuActual['Dimension'])
                        $dimensions = (array) $skuActual['Dimension'];

                    $dimensions['SkuId'] = $skuActual['Id'];
                    $dimensions['ProductId'] = $skuActual['ProductId'];
                    $dimensions['SkuName'] = $skuActual['SkuName'];

                    $lineaDimensions[] = $dimensions;
                }
                if (in_array('RealDimension', $campos)) {
                    if ($skuActual['RealDimension'])
                        $realDimensions = (array) $skuActual['RealDimension'];

                    $realDimensions['SkuId'] = $skuActual['Id'];
                    $realDimensions['ProductId'] = $skuActual['ProductId'];
                    $realDimensions['SkuName'] = $skuActual['SkuName'];

                    $lineaRealDimensions[] = $realDimensions;
                }


                if (in_array('Images', $campos)) {

                    $ImagesSku = [];
                    if ($skuActual['Images'])
                        $ImagesSku = (array) $skuActual['Images'];

                    $lineaImages[$sku]['SkuId'] = $skuActual['Id'];
                    $lineaImages[$sku]['ProductId'] = $skuActual['ProductId'];
                    $lineaImages[$sku]['SkuName'] = $skuActual['SkuName'];
                    $lineaImages[$sku]['Image URL 1'] = '';
                    $lineaImages[$sku]['Image URL 2'] = '';
                    $lineaImages[$sku]['Image URL 3'] = '';
                    $lineaImages[$sku]['Image URL 4'] = '';
                    $lineaImages[$sku]['Image URL 5'] = '';

//                    $urlApiSkuImages = $baseUrl . "/api/catalog/pvt/stockkeepingunit/" . urlencode($sku) . "/file";
//                    $request = $client->get($urlApiSkuImages, [
//                        'http_errors' => false,
//                        'headers' => $headers
//                    ]);
//                    $response = json_decode($request->getBody());
//                    $imagenResponse = (array) $response;
//                    $i = 1;
//                    foreach ($imagenResponse as $image) {
//                        $imagen = (array) $image;
//
//                        foreach ($ImagesSku as $imageSku) {
//                            if ($imagen['ArchiveId'] == $imageSku->FileId) {
//                                $imagen['url'] = $imageSku->ImageUrl;
//                            };
//                        }
//
//                        $lineaImages[$sku]['Image URL ' . $i] = $imagen['url'];
//                        $lineaImages[$sku]['IsMain' . $i] = $imagen['IsMain'];
//                        $lineaImages[$sku]['Label' . $i] = $imagen['Label'];
//                        $lineaImages[$sku]['Name' . $i] = $imagen['Name'];
//                        $lineaImages[$sku]['FileId' . $i] = $imagen['ArchiveId'];
//                        $i++;
//
//                        //$lineaImages[$skuActual['Id']][] = $imagen;
//                    }
                }
                if (in_array('Inventory', $campos)) {

//                    if ($skuActual['stocks'])
//                        $StockSku = (array) $skuActual['stocks'];
                    $urlApiSkuSStock = $baseUrl . "/api/logistics/pvt/inventory/skus/" . urlencode($sku);
                    $request = $client->get($urlApiSkuSStock, [
                        'http_errors' => false,
                        'headers' => $headers
                    ]);
                    $response = json_decode($request->getBody());
//                    $response = (array) $response;
                    //$i = 0;
//                    foreach ($response as $skuSpecification) {
//                        $lineaSpecifications[] = (array) $skuSpecification;
//                    }
                    $skuStockResponse = (array) $response;

                    foreach ($skuStockResponse['balance'] as $balance) {
                        $balance = (array) $balance;
                        $arrayStock = [];
//                        $arrayStock['skuId'] = $sku;
//                        $arrayStock['warehouseId'] = $balance['warehouseId'];

                        if ($balance['warehouseId'] == '01') {

                            $urlApiInvetorybyWh = $baseUrl . "/api/logistics/pvt/inventory/items/" . $sku . "/warehouses/" . $balance['warehouseId'];
                            $request = $client->get($urlApiInvetorybyWh, [
                                'http_errors' => false,
                                'headers' => $headers
                            ]);
                            $response = json_decode($request->getBody());
                            $invByWarehouse = (array) $response;
                            foreach ($invByWarehouse as $inventory) {
                                $inventory = (array) $inventory;
                                if ($inventory['dockId'] == 'AcusComplementos') {
                                    $arrayStock['skuId'] = $sku;
                                    $arrayStock['SkuName'] = $skuActual['SkuName'];
                                    $arrayStock['ProductId'] = $skuActual['ProductId'];

                                    $arrayStock['warehouseId'] = $balance['warehouseId'];

                                    //$arrayStock['dockId'] = $inventory['dockId'];
                                    $arrayStock['totalQuantity'] = $inventory['totalQuantity'];
                                    // $arrayStock['reservedQuantity'] = $inventory['reservedQuantity'];
                                    // $arrayStock['availableQuantity'] = $inventory['availableQuantity'];
                                    $arrayStock['isUnlimited'] = $inventory['isUnlimited'];
//                                    $arrayStock['salesChannel'] = $inventory['salesChannel'];
//                                    $arrayStock['deliveryChannels'] = $inventory['deliveryChannels'];
                                    $lineaStock[] = $arrayStock;
                                }
                            }
                        }
                    }
                }
            } catch (RequestException $e) {
                if ($e->hasResponse()) {
                    $exception = (string) $e->getResponse()->getBody();
                    $lineas[] = ['Id' => $sku, $campos[1] => $exception];
                } else {
                    $lineas[] = ['Id' => $sku, $campos[1] => $e->getMessage()];
                }
            }
        }
//        $lineaProductSpec = [];
//        if (in_array('ProductSpecifications', $campos)) {
//            foreach ($productIds as $productId) {
//                try {
//
//                    if ($skuActual['ProductSpecifications'])
//                        $SpecificationsProduc = (array) $skuActual['ProductSpecifications'];
//
//                    $urlApiProductSpecifications = $baseUrl . "/api/catalog_system/pvt/products/" . urlencode($productId) . "/specification";
//                    $request = $client->get($urlApiProductSpecifications, [
//                        'http_errors' => false,
//                        'headers' => $headers
//                    ]);
//                    $response = json_decode($request->getBody());
//
//                    $productSpecResponse = (array) $response;
//                    $i = 0;
//                    foreach ($productSpecResponse as $specification) {
//                        $specification = (array) $specification;
////                        foreach ($SpecificationsSku as $SpecificationSku) {
////                            if ($specification['FieldId'] == $SpecificationSku->FieldId) {
////                                $specification['FieldName'] = $SpecificationSku->FieldName;
////                            };
////                        }
//                        
//                        $lineaProductSpec[] = (array) $specification;
//                    }
//                } catch (RequestException $e) {
//                    if ($e->hasResponse()) {
//                        $exception = (string) $e->getResponse()->getBody();
//                        $lineas[] = ['Id' => $sku, $campos[1] => $exception];
//                    } else {
//                        $lineas[] = ['Id' => $sku, $campos[1] => $e->getMessage()];
//                    }
//                }
//            }
//        }

        if (count($lineas) > 0) {

            $file = app('App\Http\Controllers\SpreadsheetController')->exportToExcel($lineas, $campos);
            $file = app('App\Http\Controllers\SpreadsheetController')->addSheetInfoToExcel($file, $this::$colores, $this::$color, $this::$talla, $this::$tallaZapato, $this::$material, null, null);


            if (in_array('SkuSpecifications', $campos)) {
//                $skuSpeCampos = [ "Id", "SkuId", "FieldId", "FieldValueId", "Text",];
//                $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaSpecificationHide, $skuSpeCampos, 'SkuSpecificationsHide');
                $skuSpeCampos = ["SkuId", "ProductId", "SkuName", "Color", "Colores", "Talla", "Talla zapatos", "Material",];
                $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaSpecifications, $skuSpeCampos, 'SkuSpecifications');
                $file = app('App\Http\Controllers\SpreadsheetController')->addSheetValidationSkuSpec($file, 'SkuSpecifications', ["Color", "Colores", "Talla", "Talla zapatos", "Material"]);
            }
//        
            if (in_array('Images', $campos)) {

                $skuImgCampos = ["SkuId", "ProductId", "SkuName", "Image URL 1", "Image URL 2", "Image URL 3", "Image URL 4", "Image URL 5",];
                $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaImages, $skuImgCampos, 'Images');
            }
//                $skuImgCampos = ["SkuId", "ProductId", "SkuName", "Id", "ArchiveId", "Name", "IsMain", "Label","Text","url"];
//                $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaImages, $skuImgCampos, 'Images');

            if (in_array('Price', $campos)) {
                // $skuPriceCampos = ["SkuId", "ProductId", "SkuName", "itemId", "listPrice", "costPrice", "markup", "basePrice",];
                $skuPriceCampos = ["SkuId", "ProductId", "SkuName", "basePrice",];
                $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaPrice, $skuPriceCampos, 'Prices');
            }
            if (in_array('Inventory', $campos)) {
                // $skuStockCampos = ["skuId", "ProductId", "SkuName", "warehouseId", "totalQuantity", "isUnlimited", /* "reservedQuantity", "availableQuantity",  "dockId","salesChannel", "deliveryChannels", "timeToRefill", "dateOfSupplyUtc", "supplyLotId", "keepSellingAfterExpiration", "transfer" */];
                $skuStockCampos = ["skuId", "ProductId", "SkuName", "totalQuantity"];
                $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaStock, $skuStockCampos, 'Stock');
            }
//            if (in_array('ProductSpecifications', $campos)) {
//                $skuProductSpecCampos = ["skuId", "productId", "id", "name", "value"];
//                $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaProductSpec, $skuProductSpecCampos, 'ProductSpecifications');
//            }


            if (in_array('Dimension', $campos)) {
                $skuDimensionCampos = ["SkuId", "ProductId", "SkuName", "cubicweight", "height", "length", "weight", "width"];
                $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaDimensions, $skuDimensionCampos, 'Dimensions');
            }
            if (in_array('RealDimension', $campos)) {
                $skuRealDimensionCampos = ["SkuId", "ProductId", "SkuName", "realCubicWeight", "realHeight", "realLength", "realWeight", "realWidth"];
                $file = app('App\Http\Controllers\SpreadsheetController')->addSheetToExcel($file, $lineaRealDimensions, $skuRealDimensionCampos, 'RealDimensions');
            }
        }
        return json_encode($file);
    }

    protected function enviarUDP(Request $request) {
        set_time_limit(0);
        $result = [];

        $validated = $request->validate([
            'catalogo_udp' => 'required|mimes:xlsx,csv,xls|max:32000',
        ]);
        $file1 = $request->file('catalogo_udp');

        //update skus
        $updaterSku = app('App\Http\Controllers\SpreadsheetController')->generateJsonSkuUDP($file1);
        foreach ($updaterSku as $sku) {
            $result[] = $this->updateSkus($sku);
        }
        //update skuspecifications
        //$updaterSkuSpecification = app('App\Http\Controllers\SpreadsheetController')->generateJsonSkuSpecUDP($file1);
        $updaterSkuSpecification = app('App\Http\Controllers\SpreadsheetController')->generateJsonSkuSpecUDP($file1);

        foreach ($updaterSkuSpecification as $sku) {
            foreach ($sku as $skuSpec) {
                $result[] = $this->updateSkusSpecifications($skuSpec);
            }
        }

        //update Images Files
        $updaterSkuSImages = app('App\Http\Controllers\SpreadsheetController')->generateJsonSkuImagesUDP($file1);
        foreach ($updaterSkuSImages as $sku) {
            $result[] = $this->updateSkusImages($sku);
        }

        //update Images Stock
        $updaterSkuStock = app('App\Http\Controllers\SpreadsheetController')->generateJsonSkuStockUDP($file1);
        foreach ($updaterSkuStock as $sku) {
            $result[] = $this->updateSkuStock($sku);
        }
        //update price
        $updaterSkuPrice = app('App\Http\Controllers\SpreadsheetController')->generateJsonSkuPriceUDP($file1);
        foreach ($updaterSkuPrice as $sku) {
            $result[] = $this->updateSkuPrice($sku);
        }
//        //update productSpecifications
//        $updaterProductSpecifications = app('App\Http\Controllers\SpreadsheetController')->generateJsonProductSpecificationsUDP($file1);
//        foreach ($updaterProductSpecifications as $product) {
//            $result[] = $this->updateProductSpecifications($product);
//        }
        $archivo = 'log_' . date('m_d_Y_h_i_s_a').'.txt';
        $fp = fopen($archivo, 'w');
        foreach ($result as $linea) {
            if (is_array($linea)) {
                fwrite($fp, 'Status' . $linea['status'] . ': ' . $linea['SkuId'] . ': ' . $linea['mensaje'] . PHP_EOL);
            }
        };
        return json_encode($archivo);
    }

    protected function updateSkus($sku) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');

        $client = new Client();
        $url = $baseUrl . "/api/catalog/pvt/stockkeepingunit/";
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];

        $lineas = '';
        try {
            $Id = $sku['Id'];
            $request = $client->put($url . $Id, [
                'http_errors' => false,
                'headers' => $headers,
                'form_params' => $sku
            ]);

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['SkuId' => $Id, 'status' => 200, 'mensaje' => 'Sku: Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['SkuId' => $Id, 'status' => 304, 'mensaje' => 'Sku: Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['SkuId' => $Id, 'status' => 404, 'mensaje' => 'Sku no encontrado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas = ['SkuId' => $Id, 'status' => 500, 'mensaje' => $exception];
            } else {
                $lineas = ['SkuId' => $Id, 'status' => 500, 'mensaje' => $e->getMessage()];
            }
        }

        return $lineas;
    }

    protected function updateSkusImages($sku) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $skuId = $sku['skuId'];

        $client = new Client();

        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        $lineas = [];
        try {
            if ($sku['skuFileId'] <> '') {
                $url = $baseUrl . "/api/catalog/pvt/stockkeepingunit/" . $sku['skuId'] . "/file/" . $sku['skuFileId'];
                unset($sku['skuId']);
                $request = $client->put($url, [
                    'http_errors' => false,
                    'headers' => $headers,
                    'body' => json_encode($sku)
                ]);
            } else {
                $url = $baseUrl . "/api/catalog/pvt/stockkeepingunit/" . $sku['skuId'] . "/file";
                unset($sku['skuFileId']);
                unset($sku['skuId']);
                $request = $client->post($url, [
                    'http_errors' => false,
                    'headers' => $headers,
                    'body' => json_encode($sku)
                ]);
            }

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['SkuId' => $skuId, 'status' => 200, 'mensaje' => 'Image: Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['SkuId' => $skuId, 'status' => 304, 'mensaje' => 'Image: Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['SkuId' => $skuId, 'status' => 404, 'mensaje' => 'Image: Sku no encontrado'];
            } elseif ($statuscode == 500) {
                $lineas = ['SkuId' => $skuId, 'status' => 404, 'mensaje' => 'Image: Error de Servidor'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas[] = ['Id' => $skuId, 'mensaje' => $exception];
            } else {
                $lineas[] = ['Id' => $skuId, 'mensaje' => $e->getMessage()];
            }
        }
        return $lineas;
    }

    protected function updateSkusSpecifications($sku) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');

        $client = new Client();
        $url = $baseUrl . "/api/catalog/pvt/stockkeepingunit/" . $sku['SkuId'] . "/specification";
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        $lineas = [];
        try {
            if ($sku['id']) {
                $request = $client->put($url, [
                    'http_errors' => true,
                    'headers' => $headers,
                    'form_params' => $sku
                ]);
            } else {
                $request = $client->post($url, [
                    'http_errors' => true,
                    'headers' => $headers,
                    'form_params' => $sku
                ]);
            }

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['SkuId' => $sku['SkuId'], 'status' => 200, 'mensaje' => 'Specification: Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['SkuId' => $sku['SkuId'], 'status' => 304, 'mensaje' => 'Specification:  Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['SkuId' => $sku['SkuId'], 'status' => 404, 'mensaje' => 'Specification:  Sku no encontrado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas[] = ['Id' => $sku['SkuId'], 'mensaje' => $exception];
            } else {
                $lineas[] = ['Id' => $sku['SkuId'], 'mensaje' => $e->getMessage()];
            }
        }
        return $lineas;
    }

    protected function updateSkuStock($sku) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');

        $client = new Client();
//        /api/logistics/pvt/inventory/skus/skuId/warehouses/warehouseId
        $url = $baseUrl . "/api/logistics/pvt/inventory/skus/" . $sku['skuId'] . "/warehouses/" . $sku['warehouseId'];
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        $lineas = [];
        try {
            $request = $client->put($url, [
                'http_errors' => false,
                'headers' => $headers,
                'form_params' => $sku
            ]);

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['SkuId' => $sku['skuId'], 'status' => 200, 'mensaje' => 'Stock: Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['SkuId' => $sku['skuId'], 'status' => 304, 'mensaje' => 'Stock: Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['SkuId' => $sku['skuId'], 'status' => 404, 'mensaje' => 'Stock: Sku no encontrado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas[] = ['Id' => $sku['skuId'], 'mensaje' => $exception];
            } else {
                $lineas[] = ['Id' => $sku['skuId'], 'mensaje' => $e->getMessage()];
            }
        }
        return $lineas;
    }

    protected function updateSkuPrice($sku) {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $apiAcount = env('API_ACOUNTNAME');
        $apiEnviroment = env('API_ENVIROMENT');

        $client = new Client();

        $url = "https://api." . $apiEnviroment . "/" . $apiAcount . "/pricing/prices/" . urlencode($sku['itemId']);
        $skuId = $sku['itemId'];

        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/vnd.vtex.pricing.v3+json',
            "Content-Type" => "application/json"
        ];

        try {
//            dd($sku);
            unset($sku['itemId']);
            $request = $client->put($url, [
                'http_errors' => true,
                'headers' => $headers,
                'body' => json_encode($sku)
            ]);

            $statuscode = $request->getStatusCode();
            if ($statuscode == 200) {
                $response = json_decode($request->getBody());
                $lineas = ['SkuId' => $skuId, 'status' => 200, 'mensaje' => 'Price: Actualizado Correctamete'];
            } elseif ($statuscode == 304) {
                $lineas = ['SkuId' => $skuId, 'status' => 304, 'mensaje' => 'Price: Problemas de redireccion'];
            } elseif ($statuscode == 404) {
                $lineas = ['SkuId' => $skuId, 'status' => 404, 'mensaje' => 'Price: Sku no encontrado'];
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $lineas[] = ['Id' => $skuId, 'mensaje' => $exception];
            } else {
                $lineas[] = ['Id' => $skuId, 'mensaje' => $e->getMessage()];
            }
        } //catch (ClientException $e) {
//            $lineas[] = Psr7\Message::toString($e->getRequest());
//            $lineas[] = Psr7\Message::toString($e->getResponse());
//        }
        return $lineas;
    }

    public static function getSpecifications($categoryId) {
        $baseUrl = env('API_ENDPOINT');
        $apiAcount = env('API_ACOUNTNAME');
        $apiEnviroment = env('API_ENVIROMENT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $specifications = [];
        $client = new Client();
        $url = $baseUrl . "/api/catalog_system/pub/specification/field/listTreeByCategoryId/" . $categoryId;
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        try {
            $request = $client->get($url, [
                'http_errors' => true,
                'headers' => $headers
            ]);
            $response = json_decode($request->getBody());

            foreach ($response as $fieldId) {
                $fieldId = (array) $fieldId;
                if ($fieldId['IsActive']) {

                    $url = $baseUrl . "/api/catalog_system/pub/specification/fieldvalue/" . $fieldId['FieldId'];


                    $request = $client->get($url, [
                        'http_errors' => true,
                        'headers' => $headers
                    ]);
                    $response2 = json_decode($request->getBody());

                    foreach ($response2 as $fieldValue) {
                        $fieldValue = (array) $fieldValue;
                        if ($fieldValue['IsActive'] == true) {
                            $specifications[] = ['FieldId' => $fieldId['FieldId'], 'Name' => $fieldId['Name'], 'FieldValueId' => $fieldValue['FieldValueId'], 'Text' => $fieldValue['Value']];
                            if ($fieldId['Name'] == 'Colores') {
                                self::$colores[] = [$fieldValue['FieldValueId'], $fieldValue['Value']];
                            }
                            if ($fieldId['Name'] == 'Color') {
                                self::$color[] = [$fieldValue['FieldValueId'], $fieldValue['Value']];
                            }
                            if ($fieldId['Name'] == 'Talla') {
                                self::$talla[] = [$fieldValue['FieldValueId'], $fieldValue['Value']];
                            }
                            if ($fieldId['Name'] == 'Talla Zapato') {
                                self::$tallaZapato[] = [$fieldValue['FieldValueId'], $fieldValue['Value']];
                            }
                            if ($fieldId['Name'] == 'Material') {
                                self::$material[] = [$fieldValue['FieldValueId'], $fieldValue['Value']];
                            }
                        }
                    }
                }
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $response = response()->json(['status' => 'error', 'response' => $exception]);
            } else {
                $response = response()->json(['status' => 'error', 'response' => $e->getMessage()]);
            }
        }

        DB::table('specifications')->truncate();

        DB::table('specifications')
                ->Insert(
                        $specifications
        );

        return $specifications;
    }

    function importToDB(Request $request) {

        if (request()->ajax()) {
            $validated = $request->validate([
                'catalogo_file' => 'required|mimes:xlsx,csv,xls|max:50000',
            ]);
            $file1 = $request->file('catalogo_file');

            try {
                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file1);
                $worksheet = $spreadsheet->getActiveSheet();
                $row_limit = $worksheet->getHighestDataRow();
                $column_limit = $worksheet->getHighestDataColumn();
                $row_range = range(2, $row_limit);



//        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
//        $worksheet = $spreadsheet->getSheet(0); //
//        $highestRow = $worksheet->getHighestRow(); // e.g. 10
//        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
//        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
//
//        
                $column_range = range('A', $column_limit);
                $startcount = 2;
                $data = array();
                foreach ($row_range as $row) {
                    $data[] = [
                        'skuid' => $worksheet->getCell('A' . $row)->getValue(),
                        'sku_name' => $worksheet->getCell('B' . $row)->getValue(),
                        'is_active' => $worksheet->getCell('D' . $row)->getValue(),
                        'ean' => $worksheet->getCell('E' . $row)->getValue(),
                        'product_id' => $worksheet->getCell('T' . $row)->getValue(),
                        'product_name' => $worksheet->getCell('U' . $row)->getValue(),
                        'product_ref_id' => $worksheet->getCell('X' . $row)->getValue(),
                        'departament_id' => $worksheet->getCell('AI' . $row)->getValue(),
                        'departament_name' => $worksheet->getCell('AJ' . $row)->getValue(),
                        'category_id' => $worksheet->getCell('AK' . $row)->getValue(),
                        'category_name' => $worksheet->getCell('AL' . $row)->getValue(),
                        'brand_id' => $worksheet->getCell('AM' . $row)->getValue(),
                        'brand' => $worksheet->getCell('AN' . $row)->getValue(),
                    ];
                    $startcount++;
                }
                DB::table('skus')->upsert($data, ['skuid']);

                $this->categorias = $this->getCategorias();
                $this->marcas = $this->getMarcas();
                $this->specifications = $this->getSpecifications(1);
            } catch (Exception $e) {
                $error_code = $e->errorInfo[1];
                return response()->json(['status' => 'error', 'response' => $error_code]);
            }
            return response()->json(['status' => 'ok', 'response' => 'Importacion Realizada Correctamente']);
        }

//          $result['columns']= ['SkuId','ProductId','SkuName'];

        return view('admin.importdb');
    }

    public static function getCategorias() {
        $baseUrl = env('API_ENDPOINT');
        $apiAcount = env('API_ACOUNTNAME');
        $apiEnviroment = env('API_ENVIROMENT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $client = new Client();
        $url = $baseUrl . "/api/catalog_system/pub/category/tree/8";

        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
        ];
        try {
            $request = $client->get($url, [
                'http_errors' => true,
                'headers' => $headers
            ]);
            $response = json_decode($request->getBody());

            //$response = self::_getTree($response);
            $list = [];
            $list = self::_getListCategory($response, '', $list, 0);
            $listCat = [];
            $response = [];
            foreach ($list as $key => $value) {
                $listCat = ['id' => $value['id'], 'name' => trim($value['name']), 'ruta' => trim($value['ruta'])];
                DB::table('categories')->upsert($listCat, ['id']);
                $response[] = $listCat;
            }
            $column = 'ruta';
            array_multisort(array_column($response, $column), $response);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $response = response()->json(['status' => 'error', 'response' => $exception]);
            } else {
                $response = response()->json(['status' => 'error', 'response' => $e->getMessage()]);
            }
        }

        return $response;
    }

    public static function _getTree($tree) {
        $i = 0;
        $arbol = [];
        foreach ($tree as $hijo) {
//         $arbol[$i]['id'] = $hijo->id;
            $arbol[$i]['id'] = $hijo->id;
            $arbol[$i]['name'] = $hijo->name;
            if ($hijo->hasChildren) {
                $arbol[$i]['nodes'] = self::_getTree($hijo->children);
            }
//            else
//                $arbol[$i]['nodes'] = [];
            $i++;
        }
        return $arbol;
    }

    public static function _getListCategory($tree, $ruta, &$list) {

        foreach ($tree as $hijo) {
            $id = $hijo->id;
            $text = $ruta . $hijo->name;
//            $list.put([$id=>$text]);
            array_push($list, ['id' => $id, 'ruta' => $text, 'name' => $hijo->name]);
            if ($hijo->hasChildren) {
                self::_getListCategory($hijo->children, $text . '/', $list);
            }
        }
        return $list;
    }

    public static function getMarcas() {

        $baseUrl = env('API_ENDPOINT');
        $vtexKey = env('X_VTEX_API_AppKey');
        $vtexToken = env('X_VTEX_API_AppToken');
        $client = new Client();
        $url = $baseUrl . "/api/catalog_system/pvt/brand/list";
        $headers = [
            "X-VTEX-API-AppKey" => $vtexKey,
            "X-VTEX-API-AppToken" => $vtexToken,
            'Accept' => 'application/json',
        ];
        try {
            $request = $client->get($url, [
                'http_errors' => true,
                'headers' => $headers
            ]);
            $response = json_decode($request->getBody());
            $list = (array) $response;
            $response = [];
            foreach ($list as $key => $value) {
                $value = (array) $value;
                $listCat = ['id' => $value['id'], 'name' => trim($value['name'])];
                DB::table('brands')->upsert($listCat, ['id']);
                $response[] = $listCat;
            }

            //DB::table('brands')->upsert($response, ['id']);
            $column = 'name';
            array_multisort(array_column($response, $column), $response);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $response = response()->json(['status' => 'error', 'response' => $exception]);
            } else {
                $response = response()->json(['status' => 'error', 'response' => $e->getMessage()]);
            }
        }
        return $response;
    }

}
